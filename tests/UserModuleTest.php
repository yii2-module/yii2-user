<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use Yii2Module\Yii2User\UserModule;

/**
 * UserModuleTest test file.
 * 
 * @author Anastaszor
 * @covers \Yii2Module\Yii2User\UserModule
 *
 * @internal
 *
 * @small
 */
class UserModuleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UserModule
	 */
	protected UserModule $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetBootstrapName() : void
	{
		$this->assertEquals('people', $this->_object->getBootstrapIconName());
	}
	
	public function testGetLabel() : void
	{
		$this->assertEquals('User', $this->_object->getLabel());
	}
	
	public function testGetEnabledBundles() : void
	{
		$this->assertNotEmpty($this->_object->getEnabledBundles());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UserModule('user');
	}
	
}
