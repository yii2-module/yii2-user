# yii2-module/yii2-user

A module to manage user authentication mechanisms.

![coverage](https://gitlab.com/yii2-module/yii2-user/badges/main/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/yii2-module/yii2-user/badges/main/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install yii2-module/yii2-user ^8`


## Configuration

This module needs the following components to be set at the configuration level:

- 'db_user' should be a `\yii\db\Connection`

If you already have a database connection, you may use the following trick :

`'db_user' => function() { return \Yii::$app->get('db'); },`

where 'db' is the id of your database connection.


This module uses the following parameters to be set at the configuration level:

- NONE


Then the module should be configured as follows (in `console.php` or `web.php`) :

```php
$config = [
	...
	'components' => [
		...
		'user' => [
			'class' => 'yii\web\User',
			'identityClass' => 'Yii2Module\Yii2User\Components\UserIdentity',
			'enableAutoLogin' => true,
			'loginUrl' => ['user/security/signup'],
		],
		'authManager' => [
			'class' => 'Yii2Module\Yii2User\Components\RbacManager',
		],
		...
	],
	...
	'modules' => [
		...
		'user' => [
			'class' => 'Yii2Module\Yii2User\UserModule',
		],
		...
	],
	...
];
```


## Features

This module contains the following features :

| Feature Name                     | Status      |
|:--------------------------------:|:-----------:|
| Auto Deletion Unregistered Users | IMPLEMENTED |
| Email Enqueing :                 | IMPLEMENTED |
| > Inscription Confirmation Email | IMPLEMENTED |
| > Inscription Usurpation Email   | PENDING     |
| Login Form                       | IMPLEMENTED |
| Login Confirmation               | IMPLEMENTED |
| Registration Form                | IMPLEMENTED |
| Translation EN                   | PENDING     |
| Translation FR                   | PENDING     |
| Management of Users :            | PENDING     |
| > Change of user status          | PENDING     |
| > Assignment of user to group    | PENDING     |
| > Removal of user from group     | PENDING     |
| > Assignment of access to user   | PENDING     |
| > Removal of access to user      | PENDING     |
| > Ban of users                   | PENDING     |
| > Force user to reset password   | PENDING     |
| > > Reset pwd information email  | PENDING     |
| > View all groups having user    | PENDING     |
| > View all accesses having user  | PENDING     |
| Management of Groups :           | PENDING     |
| > Group Creation                 | PENDING     |
| > Group Modification             | PENDING     |
| > > Assignment of user to group  | PENDING     |
| > > Removal of user to group     | PENDING     |
| > > Assignment access to group   | PENDING     |
| > > Removal of access to group   | PENDING     |
| > Group Suppression              | PENDING     |
| > View all users of group        | PENDING     |
| > View all accesses of group     | PENDING     |
| Management of Accesses :         | PENDING     |
| > Access Creation from Modules   | PENDING     |
| > Assignment of access to users  | PENDING     |
| > > Assignment information email | PENDING     |
| > > Removal information email    | PENDING     |
| > Assignment of access to groups | PENDING     |
| > > Assignment information email | PENDING     |
| > > Removal information email    | PENDING     |
| > View all groups having access  | PENDING     |
| > View all users having access   | PENDING     |


Here is the list of modules that served as inspiration source :

[https://packagist.org/explore/?query=yii2-user](https://packagist.org/explore/?query=yii2-user)


- [https://github.com/2amigos/yii2-usuario](https://github.com/2amigos/yii2-usuario)
- [https://github.com/amnah/yii2-user](https://github.com/amnah/yii2-user)
- [https://github.com/communityii/yii2-user](https://github.com/communityii/yii2-user)
- [https://github.com/cinghie/yii2-user-extended](https://github.com/cinghie/yii2-user-extended)
- [https://github.com/dektrium/yii2-user](https://github.com/dektrium/yii2-user)
- [https://github.com/johnitvn/yii2-user-plus](https://github.com/johnitvn/yii2-user-plus)
- [https://github.com/krivochenko/yii2-users](https://github.com/krivochenko/yii2-users)
- [https://github.com/nkostadinov/yii2-user](https://github.com/nkostadinov/yii2-user)
- [https://github.com/portalium/yii2-user](https://github.com/portalium/yii2-user)
- [https://github.com/thefx/yii2-user](https://github.com/thefx/yii2-user)
- [https://github.com/vova07/yii2-users-module](https://github.com/vova07/yii2-users-module)
- [https://github.com/wdmg/yii2-users](https://github.com/wdmg/yii2-users)
- [https://github.com/yii2mod/yii2-user](https://github.com/yii2mod/yii2-user)
- [https://github.com/yiier/yii2-user-setting](https://github.com/yiier/yii2-user-setting)


## License

MIT (See [license file](LICENSE))
