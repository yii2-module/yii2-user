<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User;

use DateInterval;
use DateMalformedIntervalStringException;
use DateTimeImmutable;
use PhpExtended\Uuid\UuidV6Factory;
use Throwable;
use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\Record;
use Yii2Module\Helper\BootstrappedModule;
use Yii2Module\Yii2User\Models\AccessGroupAssignment;
use Yii2Module\Yii2User\Models\AccessHierarchy;
use Yii2Module\Yii2User\Models\AccessRole;
use Yii2Module\Yii2User\Models\AccessRule;
use Yii2Module\Yii2User\Models\AccessStatus;
use Yii2Module\Yii2User\Models\AccessUserAssignment;
use Yii2Module\Yii2User\Models\GroupGroup;
use Yii2Module\Yii2User\Models\GroupHierarchy;
use Yii2Module\Yii2User\Models\GroupStatus;
use Yii2Module\Yii2User\Models\GroupUserAssignment;
use Yii2Module\Yii2User\Models\GroupVisibility;
use Yii2Module\Yii2User\Models\MailQueue;
use Yii2Module\Yii2User\Models\UserAccount;
use Yii2Module\Yii2User\Models\UserMetadata;
use Yii2Module\Yii2User\Models\UserStatus;
use Yii2Module\Yii2User\Models\UserUser;

/**
 * UserModule class file.
 * 
 * This module is to represent the data of users, and to expand such data to
 * other databases as a reference point. This module also takes care of all
 * the potential authentication schemes that a user may use, registers
 * authentication events, manages passwords or access tokens from third party
 * authenticators, etc.
 * 
 * Its aim is to do all the functionalities that all other yii2-user modules
 * may provide with seamless integration, and configuration if needed, but with
 * secure defaults.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class UserModule extends BootstrappedModule
{
	
	/**
	 * The default route. (dashboard/index).
	 * 
	 * @var string
	 */
	public $defaultRoute = 'dashboard';
	
	/**
	 * The email from to use when mails are sent.
	 * 
	 * @var string
	 */
	public string $emailFrom = 'no-reply@example.com';
	
	/**
	 * Whether emails function is available to be used to register users and
	 * other functions.
	 * 
	 * @var boolean
	 */
	public bool $useEmails = true;
	
	/**
	 * The route to redirect after successful login attempt.
	 * 
	 * @var string
	 */
	public string $routeRedirectAfterLoginSuccess = '/site/index';
	
	// ---- ---- ---- ---- Module API ---- ---- ---- ---- \\
	
	/**
	 * Enqueues an email to be processed and sent.
	 * 
	 * @param string $emailFrom
	 * @param string $emailTo
	 * @param string $subject
	 * @param string $contents
	 * @param integer $maxRetries
	 * @param integer $retryEverySec
	 * @return boolean true if email is correctly enqueued
	 * @throws \Random\RandomException
	 * @throws DateMalformedIntervalStringException
	 */
	public function enqueueEmail(
		string $emailFrom,
		string $emailTo,
		string $subject,
		string $contents,
		int $maxRetries = 10,
		int $retryEverySec = 3600
	) : bool {
		$uuidFactory = new UuidV6Factory();
		$enqueue = new MailQueue();
		$enqueue->mail_queue_id = $uuidFactory->create()->__toString();
		$enqueue->max_attempts = $maxRetries;
		$enqueue->cur_attempts = 0;
		$enqueue->attempt_interval = $retryEverySec;
		/** @psalm-suppress MissingThrowsDocblock */
		$dateInterval = new DateInterval('P0DT'.((string) $retryEverySec).'S');
		$enqueue->datetime_next_attempt = (new DateTimeImmutable())->add($dateInterval)->format('Y-m-d H:i:s');
		$enqueue->read_count = 0;
		$enqueue->email_from = $emailFrom;
		$enqueue->email_to = $emailTo;
		$enqueue->subject = $subject;
		$enqueue->message = $contents;
		
		try
		{
			if(!$enqueue->save())
			{
				$errors = [];
				
				foreach($enqueue->getErrorSummary(true) as $error)
				{
					$errors[] = (string) $error;
				}
				
				$message = 'Failed to save email queue {id} : {err}';
				$context = [
					'id' => $enqueue->mail_queue_id,
					'err' => \implode("\n", $errors),
				];
				$this->getLogger()->critical($message, $context);
			}
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to save email queue {id} : {err}';
			$context = [
				'id' => $enqueue->mail_queue_id,
				'err' => $exc->getMessage(),
			];
			$this->getLogger()->critical($message, $context);
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'people';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return BaseYii::t('UserModule.Module', 'User');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 */
	public function getEnabledBundles() : array
	{
		return [
			'access' => new Bundle(BaseYii::t('UserModule.Module', 'Access'), [
				'group' => (new Record(AccessGroupAssignment::class, 'group', BaseYii::t('UserModule.Module', 'Access Group')))->enableFullAccess(),
				'hierarchy' => (new Record(AccessHierarchy::class, 'hierarchy', BaseYii::t('UserModule.Module', 'Access Hierarchy')))->enableFullAccess(),
				'role' => (new Record(AccessRole::class, 'role', BaseYii::t('UserModule.Module', 'Access Role')))->enableFullAccess(),
				'rule' => (new Record(AccessRule::class, 'rule', BaseYii::t('UserModule.Module', 'Access Rule')))->enableFullAccess(),
				'status' => (new Record(AccessStatus::class, 'status', BaseYii::t('UserModule.Module', 'Access Status')))->enableFullAccess(),
				'user' => (new Record(AccessUserAssignment::class, 'user', BaseYii::t('UserModule.Module', 'Access User')))->enableFullAccess(),
			]),
			'group' => new Bundle(BaseYii::t('UserModule.Module', 'Group'), [
				'group' => (new Record(GroupGroup::class, 'group', BaseYii::t('UserModule.Module', 'Group')))->enableFullAccess(),
				'hierarchy' => (new Record(GroupHierarchy::class, 'hierarchy', BaseYii::t('UserModule.Module', 'Group Hierarchy')))->enableFullAccess(),
				'status' => (new Record(GroupStatus::class, 'status', BaseYii::t('UserModule.Module', 'Group Status')))->enableFullAccess(),
				'user' => (new Record(GroupUserAssignment::class, 'user', BaseYii::t('UserModule.Module', 'Group User')))->enableFullAccess(),
				'visibility' => (new Record(GroupVisibility::class, 'visibility', BaseYii::t('UserModule.Module', 'Group Visibility')))->enableFullAccess(),
			]),
			'user' => new Bundle(BaseYii::t('UserModule.Module', 'User'), [
				'account' => (new Record(UserAccount::class, 'account', BaseYii::t('UserModule.Module', 'User Account')))->enableFullAccess(),
				'metadata' => (new Record(UserMetadata::class, 'metadata', BaseYii::t('UserModule.Module', 'User Metadata')))->enableFullAccess(),
				'status' => (new Record(UserStatus::class, 'status', BaseYii::t('UserModule.Module', 'User Status')))->enableFullAccess(),
				'user' => (new Record(UserUser::class, 'user', BaseYii::t('UserModule.Module', 'User')))->enableFullAccess(),
				'mail' => (new Record(MailQueue::class, 'mail', BaseYii::t('UserModule.Module', 'Mail Queue')))->enableFullAccess(),
			]),
		];
	}
	
}
