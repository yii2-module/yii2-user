<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Forms;

use PhpExtended\Email\EmailAddressParser;
use PhpExtended\Parser\ParseThrowable;
use yii\base\Model;
use yii\BaseYii;
use yii\validators\EmailValidator;
use yii\validators\FilterValidator;
use yii\validators\InlineValidator;
use yii\validators\RequiredValidator;
use yii\validators\StringValidator;
use Yii2Module\Yii2User\Components\PasswordStrenghValidator;

/**
 * SignupForm class file.
 * 
 * This form receives information from a future user upon registration.
 * 
 * @author Anastaszor
 */
class SignupForm extends Model
{
	
	/**
	 * The email of the user.
	 * 
	 * @var ?string
	 */
	public ?string $email = null;
	
	/**
	 * The password of the user.
	 * 
	 * @var ?string
	 */
	public ?string $password = null;
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::rules()
	 * @return array<integer, array<integer|string, integer|string|array<integer, string>>>
	 */
	public function rules() : array
	{
		return [
			[['email', 'password'], RequiredValidator::class],
			[['email', 'password'], FilterValidator::class, 'filter' => 'trim'],
			[['email'], EmailValidator::class],
			[['email'], StringValidator::class, 'min' => 3, 'max' => 50],
			[['password'], PasswordStrenghValidator::class],
			[['email'], 'canonicalizeEmail'],
		];
	}
	
	/**
	 * Canonicalizes the email value.
	 * 
	 * @param ?string $attribute
	 * @param array<integer|string, null|boolean|integer|float|string> $params
	 * @param ?InlineValidator $validator
	 * @throws \yii\base\InvalidCallException
	 * @throws \yii\base\UnknownPropertyException
	 * @SuppressWarnings("PHPMD.UnusedFormalParameter")
	 */
	public function canonicalizeEmail(?string $attribute = null, array $params = [], ?InlineValidator $validator = null) : void
	{
		try
		{
			$parser = new EmailAddressParser();
			$email = $parser->parse((string) $this->{$attribute});
			$this->{$attribute} = $email->getCanonicalRepresentation();
		}
		catch(ParseThrowable $exc)
		{
			BaseYii::error($exc->getMessage()."\n\n".$exc->getTraceAsString(), 'UserModule.SignupForm');
			$this->addError((string) $attribute, BaseYii::t('UserModule.SignupForm', 'Invalid email address'));
		}
	}
	
}
