<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Forms;

use yii\base\Model;
use yii\BaseYii;
use Yii2Module\Yii2User\Components\UserIdentity;
use Yii2Module\Yii2User\Components\UserUserManager;
use Yii2Module\Yii2User\Models\UserAccount;
use Yii2Module\Yii2User\Models\UserUser;

/**
 * LoginForm is the model behind the login form.
 *
 * @property ?UserUser $user This property is read-only.
 */
class LoginForm extends Model
{
	
	/**
	 * The email of the user.
	 * 
	 * @var ?string
	 */
	public ?string $email = null;
	
	/**
	 * The password of the user.
	 * 
	 * @var ?string
	 */
	public ?string $password = null;
	
	/**
	 * Whether to remember the user.
	 * 
	 * @var boolean
	 */
	public bool $rememberMe = true;
	
	/**
	 * @var ?UserUser
	 */
	private ?UserUser $_user = null;
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::rules()
	 * @return array<integer, array<integer|string, integer|string|array<integer, string>>>
	 */
	public function rules()
	{
		return [
			// username and password are both required
			[['email', 'password'], 'required'],
			// rememberMe must be a boolean value
			['rememberMe', 'boolean'],
			// password is validated by validatePassword()
			['password', 'validatePassword'],
		];
	}

	/**
	 * Validates the password.
	 * This method serves as the inline validation for password.
	 *
	 * @param string $attribute the attribute currently being validated
	 * @param array<string, string> $params the additional name-value pairs given in the rule
	 * @throws \yii\base\UnknownMethodException
	 * @SuppressWarnings("PHPMD.UnusedFormalParameter")
	 */
	public function validatePassword($attribute, $params) : void
	{
		if(!$this->hasErrors())
		{
			$user = $this->getUser();
			if(null !== $user)
			{
				/** @var UserUser $user */
				$account = $user->getUserAccounts()->andWhere([
					'provider_name' => UserUserManager::USER_ACCOUNT_PROVIDER_ID,
				])->one();
				if(null !== $account && $account instanceof UserAccount)
				{
					if(!\password_verify((string) $this->password, $account->access_token))
					{
						$this->addError($attribute, 'Incorrect username or password.');
					}
				}
			}
		}
	}
	
	/**
	 * Logs in a user using the provided username and password.
	 * 
	 * @return bool whether the user is logged in successfully
	 */
	public function login() : bool
	{
		try
		{
			if($this->validate())
			{
				$user = $this->getUser();
				if(null !== $user)
				{
					/** @var UserUser $user */
					if(UserUserManager::USER_STATUS_ACTIVE === $user->user_status_id
						&& null === $user->meta_deleted_at
					) {
						/** @var \yii\web\Application $app */
						$app = BaseYii::$app;
						$app->user->login(new UserIdentity($user), $this->rememberMe ? 3600 * 24 * 30 : 0);
						
						return true;
					}
				}
			}
		}
		catch(\yii\base\InvalidArgumentException $exc)
		{
			BaseYii::error(\strtr('Failed to log in with user {user} : {exc}', [
				'{user}' => $this->email,
				'{exc}' => $exc->getMessage(),
			]));
		}
		
		return false;
	}
	
	/**
	 * Finds user by [[username]].
	 *
	 * @return ?UserUser
	 */
	public function getUser()
	{
		if(null === $this->_user)
		{
			$this->_user = UserUser::findOne([
				'email_hash' => (string) \hash('sha512', (string) $this->email),
			]);
		}
		
		return $this->_user;
	}
	
}
