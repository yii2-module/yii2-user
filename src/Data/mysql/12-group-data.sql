/**
 * Database base data required by UserModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-user
 * @license MIT
 */

INSERT INTO `group_status` (`group_status_id`, `libelle_en`) VALUES
('ACT', 'Active'),
('BAN', 'Banned'),
('INA', 'Inactive'),
('PEN', 'Pending');

INSERT INTO `group_visibility` (`group_visibility_id`, `libelle_en`) VALUES
('PUB', 'Public'),
('PRV', 'Private'),
('SEC', 'Secret');
