/**
 * Database relations required by UserModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-user
 * @license MIT
 */

ALTER TABLE `user_account` ADD CONSTRAINT `fk_user_account_user_user` FOREIGN KEY (`user_user_id`) REFERENCES `user_user`(`user_user_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `user_user` ADD CONSTRAINT `fk_user_user_user_status` FOREIGN KEY (`user_status_id`) REFERENCES `user_status`(`user_status_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
