/**
 * Database relations required by UserModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-user
 * @license MIT
 */

ALTER TABLE `group_assignment` ADD CONSTRAINT `fk_group_assignment_user_user` FOREIGN KEY (`user_user_id`) REFERENCES `user_user`(`user_user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `group_assignment` ADD CONSTRAINT `fk_group_assignment_group_group` FOREIGN KEY (`group_group_id`) REFERENCES `group_group`(`group_group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `group_group` ADD CONSTRAINT `fk_group_group_group_status` FOREIGN KEY (`group_status_id`) REFERENCES `group_status`(`group_status_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `group_group` ADD CONSTRAINT `fk_group_group_group_visibility` FOREIGN KEY (`group_visibility_id`) REFERENCES `group_visibility`(`group_visibility_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `group_hierarchy` ADD CONSTRAINT `fk_group_hierarchy_group_parent` FOREIGN KEY (`group_parent_id`) REFERENCES `group_group`(`group_group_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `group_hierarchy` ADD CONSTRAINT `fk_group_hierarchy_group_child` FOREIGN KEY (`group_child_id`) REFERENCES `group_group`(`group_group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

