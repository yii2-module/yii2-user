/**
 * Database relations required by UserModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-user
 * @license MIT
 */

ALTER TABLE `access_group_assignment` ADD CONSTRAINT `fk_access_group_assignment_access_role` FOREIGN KEY (`access_role_id`) REFERENCES `access_role`(`access_role_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `access_group_assignment` ADD CONSTRAINT `fk_access_group_assignment_group_group` FOREIGN KEY (`group_group_id`) REFERENCES `group_group`(`group_group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `access_hierarchy` ADD CONSTRAINT `fk_access_hierarchy_access_role_parent` FOREIGN KEY (`access_role_parent_id`) REFERENCES `access_role`(`access_role_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `access_hierarchy` ADD CONSTRAINT `fk_access_hierarchy_access_role_child` FOREIGN KEY (`access_role_child_id`) REFERENCES `access_role`(`access_role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `access_role` ADD CONSTRAINT `fk_access_role_access_status` FOREIGN KEY (`access_status_id`) REFERENCES `access_status`(`access_status_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `access_rule_assignment` ADD CONSTRAINT `fk_access_rule_assignment_role` FOREIGN KEY (`access_role_id`) REFERENCES `access_role`(`access_role_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `access_rule_assignment` ADD CONSTRAINT `fk_access_rule_assignment_rule` FOREIGN KEY (`access_rule_id`) REFERENCES `access_rule`(`access_rule_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `access_user_assignment` ADD CONSTRAINT `fk_access_user_assignment_access_role` FOREIGN KEY (`access_role_id`) REFERENCES `access_role`(`access_role_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `access_user_assignment` ADD CONSTRAINT `fk_access_user_assignment_user_user` FOREIGN KEY (`user_user_id`) REFERENCES `user_user`(`user_user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

