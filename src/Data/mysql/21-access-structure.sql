/**
 * Database structure required by UserModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-user
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `access_role`;
DROP TABLE IF EXISTS `access_rule`;
DROP TABLE IF EXISTS `access_hierarchy`;
DROP TABLE IF EXISTS `access_status`;
DROP TABLE IF EXISTS `access_group_assignment`;
DROP TABLE IF EXISTS `access_user_assignment`;
DROP TABLE IF EXISTS `access_rule_assignment`;

SET foreign_key_checks = 1;


CREATE TABLE `access_role`
(
	`access_role_id` CHAR(36) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The unique id of the role',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	`access_status_id` CHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The related id of the status for this role',
	`module_role_id` VARCHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The identifier of the role for the module that created it',
	`min_ring_level` SMALLINT(3) NOT NULL DEFAULT 0 COMMENT 'The minimum authentication level to execute this rule',
	`libelle_en` VARCHAR(65) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the role'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the access roles';

CREATE TABLE `access_rule`
(
	`access_rule_id` CHAR(36) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The unique id of the role',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	`module_rule_id` VARCHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The identifier of the rule for the module that created it',
	`json_data` TEXT NOT NULL DEFAULT '{}' COMMENT 'The data for the rule to be executed'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the access rules';

CREATE TABLE `access_hierarchy`
(
	`access_role_parent_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The id of the parent role',
	`access_role_child_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The id of the child role',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	PRIMARY KEY (`access_role_parent_id`, `access_role_child_id`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the access hierarchy';

CREATE TABLE `access_status`
(
	`access_status_id` CHAR(3) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the access status',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	`libelle_en` VARCHAR(64) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the status'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the access statuses';

CREATE TABLE `access_group_assignment`
(
	`access_role_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The id of the access assignment',
	`group_group_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The id of the group',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	PRIMARY KEY (`access_role_id`, `group_group_id`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the access assignments to groups';

CREATE TABLE `access_user_assignment`
(
	`access_role_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The id of the access assignment',
	`user_user_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The id of the user',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	PRIMARY KEY (`access_role_id`, `user_user_id`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the access assignments to users';

CREATE TABLE `access_rule_assignment`
(
	`access_role_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The id of the access role',
	`access_rule_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The id of the access rule',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	PRIMARY KEY (`access_role_id`, `access_rule_id`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the access rules to roles';
