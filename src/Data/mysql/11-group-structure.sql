/**
 * Database structure required by UserModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-user
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `group_user_assignment`;
DROP TABLE IF EXISTS `group_group`;
DROP TABLE IF EXISTS `group_hierarchy`;
DROP TABLE IF EXISTS `group_status`;
DROP TABLE IF EXISTS `group_visibility`;

SET foreign_key_checks = 1;

CREATE TABLE `group_user_assignment`
(
	`group_group_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The unique id of the related group',
	`user_user_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The unique id of the user',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	PRIMARY KEY (`user_user_id`, `group_group_id`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the user assignment to groups';

CREATE TABLE `group_group`
(
	`group_group_id` CHAR(36) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the group',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	`group_status_id` CHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related status',
	`group_visibility_id` CHAR(3) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related visibility',
	`libelle_en` VARCHAR(64) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The name of the group'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all group data';

CREATE TABLE `group_hierarchy`
(
	`group_parent_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The id of the parent group',
	`group_child_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The id of the child group',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	PRIMARY KEY (`group_parent_id`, `group_child_id`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the group hierarchies';

CREATE TABLE `group_status`
(
	`group_status_id` CHAR(3) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The unique id of the status',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	`libelle_en` VARCHAR(64) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The name of the status'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all group status';

CREATE TABLE `group_visibility`
(
	`group_visibility_id` CHAR(3) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The unique id of the visibility',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	`libelle_en` VARCHAR(64) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The name of the visibility'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all group visibility';

