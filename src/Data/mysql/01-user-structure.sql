/**
 * Database structure required by UserModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-user
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `mail_queue`;
DROP TABLE IF EXISTS `user_metadata`;
DROP TABLE IF EXISTS `user_user`;
DROP TABLE IF EXISTS `user_account`;
DROP TABLE IF EXISTS `user_status`;

SET foreign_key_checks = 1;


CREATE TABLE `mail_queue`
(
	`mail_queue_id` CHAR(36) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The unique id of the enqueued mail',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	`cur_thread_pid` INT(11) DEFAULT NULL COMMENT 'The pid of the thread that is currently processing this record (for concurrency)',
	`max_attempts` SMALLINT(3) NOT NULL DEFAULT 10 COMMENT 'The maximum number of retries',
	`cur_attempts` SMALLINT(3) NOT NULL DEFAULT 0 COMMENT 'The current number of tries',
	`attempt_interval` INT(11) NOT NULL DEFAULT 3600 COMMENT 'The interval, in seconds, between two attempts',
	`datetime_next_attempt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The date and time when the next attempt should be done',
	`datetime_last_attempt` DATETIME DEFAULT NULL COMMENT 'The date and time when the last attempt was made to send this email',
	`datetime_sent` DATETIME DEFAULT NULL COMMENT 'The date and time when the email was successfully sent',
	`datetime_last_read` DATETIME DEFAULT NULL COMMENT 'The date and time when this email was read',
	`read_count` SMALLINT(3) NOT NULL DEFAULT 0 COMMENT 'The number of times this email was read',
	`email_from` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The email to put in the FROM field',
	`email_to` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The email to put in the TO field',
	`subject` VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The subject to send with the email',
	`message` MEDIUMTEXT NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The contents of the email',
	INDEX `mail_queue_wasnt_sent` (`datetime_sent`, `datetime_next_attempt`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the emails that were enqueued';

CREATE TABLE `user_metadata`
(
	`user_metadata_id` VARCHAR(250) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The id of the metadata',
	`contents` VARCHAR(250) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The value of the setting'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the settings for the module';

CREATE TABLE `user_user`
(
	`user_user_id` CHAR(36) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The unique uuid of the user',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	`meta_deleted_at` DATETIME DEFAULT NULL COMMENT 'Metadata for the soft deletion date of this record',
	`user_status_id` CHAR(3) NOT NULL COLLATE ascii_bin DEFAULT 'PEN' COMMENT 'The status of the user',
	`username_hash` CHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The sha512 of the username',
	`username` VARCHAR(50) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The name that has been choosen by the user',
	`email_hash` CHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The sha512 of the user email',
	`email_canonical` VARCHAR(50) NOT NULL COLLATE ascii_bin COMMENT 'The canonicalized email of the user',
	`date_confirmed_at` DATETIME DEFAULT NULL COMMENT 'The date when the user was confirmed',
	`registered_ip` VARCHAR(45) NOT NULL DEFAULT 'NOT_PROVIDED' COMMENT 'The ip address of the user',
	INDEX `user_user_username_hash` (`username_hash`),
	UNIQUE INDEX `user_user_email_hash` (`email_hash`),
	INDEX `user_user_user_status` (`user_status`),
	INDEX `user_user_date_confirmed_at` (`date_confirmed_at`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the user data';

CREATE TABLE `user_account`
(
	`user_account_id` CHAR(36) NOT NULL COLLATE ascii_bin PRIMARY KEY COMMENT 'The unique uuid of the user account',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	`meta_deleted_at` DATETIME DEFAULT NULL COMMENT 'Metadata for the soft deletion date of this record',
	`user_user_id` CHAR(36) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related user',
	`provider_name` VARCHAR(20) NOT NULL COLLATE ascii_bin DEFAULT 'PASSWORD' COMMENT 'The provider of the account',
	`user_provider_id` VARCHAR(50) NOT NULL COLLATE ascii_bin COMMENT 'The user id that the provided uses to authenticate this user',
	`access_token` VARCHAR(100) NOT NULL COLLATE ascii_bin COMMENT 'The token or hashed password of the user',
	`token_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When the token was created the last time',
	`token_expires_at` DATETIME NOT NULL COMMENT 'The date when the token expires'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the user account data';

CREATE TABLE `user_status`
(
	`user_status_id` CHAR(3) NOT NULL PRIMARY KEY COMMENT 'The unique id of the status',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation date of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata for the last updated date of this record',
	`meta_deleted_at` DATETIME DEFAULT NULL COMMENT 'Metadata for the soft deletion date of this record',
	`libelle_en` VARCHAR(50) NOT NULL COLLATE utf8mb4_general_ci COMMENT 'The libelle of the user status'
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the user status data';
