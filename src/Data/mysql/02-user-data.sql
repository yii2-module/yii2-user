/**
 * Database base data required by UserModule.
 *
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-user
 * @license MIT
 */

INSERT INTO `user_status` (`user_status_id`, `libelle_en`) VALUES
('ACT', 'Active'),
('BAN', 'Banned'),
('INA', 'Inactive'),
('PEN', 'Pending');

