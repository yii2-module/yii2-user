<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use yii\BaseYii;
use yii\validators\Validator;
use Yii2Module\Yii2User\Models\UserMetadata;

/**
 * PasswordStrenghValidator class file.
 * 
 * This class validates password values to follow certain rules.
 * 
 * @author Anastaszor
 * @psalm-suppress PropertyNotSetInConstructor
 */
class PasswordStrenghValidator extends Validator
{
	
	/**
	 * The min number of total characters that the password must have.
	 * 
	 * @var integer
	 */
	protected int $_minNbChars = 14;
	
	/**
	 * The min number of lower characters that the password must have.
	 * 
	 * @var integer
	 */
	protected int $_minNbLower = 1;
	
	/**
	 * The min number of upper characters that the password must have.
	 * 
	 * @var integer
	 */
	protected int $_minNbUpper = 1;
	
	/**
	 * The min number of digits characters that the password must have.
	 * 
	 * @var integer
	 */
	protected int $_minNbDigit = 1;
	
	/**
	 * The min number of special characters that the password must have.
	 * 
	 * @var integer
	 */
	protected int $_minNbSpecial = 1;
	
	/**
	 * Builds a new PasswordStrenghValidator with the current module
	 * configuration.
	 */
	public function __construct()
	{
		$minNbCharsRecord = UserMetadata::findOne(__CLASS__.'.minNbChars');
		if(null !== $minNbCharsRecord)
		{
			$this->_minNbChars = \max(0, (int) $minNbCharsRecord->contents);
		}
		
		$minNbLowerRecord = UserMetadata::findOne(__CLASS__.'.minNbLower');
		if(null !== $minNbLowerRecord)
		{
			$this->_minNbLower = \max(0, (int) $minNbLowerRecord->contents);
		}
		
		$minNbUpperRecord = UserMetadata::findOne(__CLASS__.'.minNbUpper');
		if(null !== $minNbUpperRecord)
		{
			$this->_minNbUpper = \max(0, (int) $minNbUpperRecord->contents);
		}
		
		$minNbDigitRecord = UserMetadata::findOne(__CLASS__.'.minNbDigit');
		if(null !== $minNbDigitRecord)
		{
			$this->_minNbDigit = \max(0, (int) $minNbDigitRecord->contents);
		}
		
		$minNbSpecialRecord = UserMetadata::findOne(__CLASS__.'.minNbSpecial');
		if(null !== $minNbSpecialRecord)
		{
			$this->_minNbSpecial = \max(0, (int) $minNbSpecialRecord->contents);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\validators\Validator::validateValue()
	 * @param null|boolean|integer|float|string $value
	 * @return array<integer, string|array<string, integer|string>>
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	protected function validateValue($value) : array
	{
		$value = (string) $value;
		$countNbChars = \mb_strlen($value);
		$countNbLower = (int) \preg_match_all('#[a-z]#', $value);
		$countNbUpper = (int) \preg_match_all('#[A-Z]#', $value);
		$countNbDigit = (int) \preg_match_all('#[0-9]#', $value);
		$countNbSpecial = (int) \preg_match_all('#[ -/:-@\\[-`}-~]#', $value);
		
		$message = [];
		$context = [];
		
		if($countNbChars < $this->_minNbChars)
		{
			$message[] = BaseYii::t(
				'UserModule.Components',
				'{attribute} should contain at least {n, plural, one{one character}, other{# characters}} ({found1} found)',
				['n' => $this->_minNbChars],
			);
			$context['found1'] = $countNbChars;
		}
		
		if($countNbLower < $this->_minNbLower)
		{
			$message[] = BaseYii::t(
				'UserModule.Components',
				'{attribute} should contain at least {n, plural, one{one lower case character}, other{# lower case characters}} ({found2} found)',
				['n' => $this->_minNbLower],
			);
			$context['found2'] = $countNbLower;
		}
		
		if($countNbUpper < $this->_minNbUpper)
		{
			$message[] = BaseYii::t(
				'UserModule.Components',
				'{attribute} should contain at least {n, plural, one{one upper case character}, other{# upper case characters}} ({found3} found)',
				['n' => $this->_minNbUpper],
			);
			$context['found3'] = $countNbUpper;
		}
		
		if($countNbDigit < $this->_minNbDigit)
		{
			$message[] = BaseYii::t(
				'UserModule.Components',
				'{attribute} should contain at least {n, plural, one{one numeric character}, other{# numeric characters}} ({found4} found)',
				['n' => $this->_minNbDigit],
			);
			$context['foundd4'] = $countNbDigit;
		}
		
		if($countNbSpecial < $this->_minNbSpecial)
		{
			$message[] = BaseYii::t(
				'UserModule.Components',
				'{attribute} should contain at least {n, plural, one{one special character}, other{# special characters}} ({found5} found)',
				['n' => $this->_minNbSpecial],
			);
			$context['found5'] = $countNbSpecial;
		}
		
		return empty($message) ? [] : [\implode("\n", $message), $context];
	}
	
}
