<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use PhpExtended\Rbac\RoleInterface;
use PhpExtended\Rbac\RoleStatusInterface;
use PhpExtended\Rbac\Rule;
use Yii2Module\Yii2User\Models\AccessRole;

/**
 * RbacRole class file.
 * 
 * This class is a simple implementation based on the record of this module.
 * 
 * @author Anastaszor
 */
class RbacRole implements RoleInterface
{
	
	/**
	 * The underlying record.
	 * 
	 * @var AccessRole
	 */
	protected AccessRole $_record;
	
	/**
	 * Builds a new RbacRole with the underlying record.
	 * 
	 * @param AccessRole $record
	 */
	public function __construct(AccessRole $record)
	{
		$this->_record = $record;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->getIdentifier();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return (string) $this->_record->access_role_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getRolename()
	 */
	public function getRolename() : string
	{
		return (string) $this->_record->libelle_en;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getRoleStatus()
	 */
	public function getRoleStatus() : RoleStatusInterface
	{
		return new RbacRoleStatus($this->_record->accessStatus);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getRules()
	 */
	public function getRules() : array
	{
		$rules = [];
		
		foreach($this->_record->accessRules as $rule)
		{
			$decoded = (array) \json_decode($rule->json_data);
			$data = [];
			
			foreach($decoded as $key => $value)
			{
				$data[(string) $key] = (string) $value;
			}
			
			$rules[] = new Rule(
				$rule->access_rule_id,
				$rule->module_rule_id.'/'.$rule->access_rule_id,
				$data,
			);
		}
		
		return $rules;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getAssignedUsers()
	 */
	public function getAssignedUsers() : array
	{
		$users = [];
		
		foreach($this->_record->userUsers as $user)
		{
			$users[] = new RbacUser($user);
		}
		
		return $users;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getAssignedGroups()
	 */
	public function getAssignedGroups() : array
	{
		$groups = [];
		
		foreach($this->_record->groupGroups as $group)
		{
			$groups[] = new RbacGroup($group);
		}
		
		return $groups;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getParentRoles()
	 */
	public function getParentRoles() : array
	{
		$roles = [];
		
		foreach($this->_record->accessRoleParents as $role)
		{
			$roles[] = new RbacRole($role);
		}
		
		return $roles;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getParentRolesRecursive()
	 */
	public function getParentRolesRecursive() : array
	{
		/** @var array<string, AccessRole> $roles */
		$roles = [];
		
		$stack = $this->_record->accessRoleParents;
		
		while(!empty($stack))
		{
			/** @var AccessRole $current */
			$current = \array_shift($stack);
			if(isset($roles[$current->access_role_id]))
			{
				continue;
			}
			
			$roles[$current->access_role_id] = $current;
			$stack = \array_merge($stack, $current->accessRoleParents);
		}
		
		$newRoles = [];
		
		foreach($roles as $role)
		{
			$newRoles[] = new RbacRole($role);
		}
		
		return $newRoles;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getChildrenRoles()
	 */
	public function getChildrenRoles() : array
	{
		$roles = [];
		
		foreach($this->_record->accessRoleChildren as $role)
		{
			$roles[] = new RbacRole($role);
		}
		
		return $roles;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getChildrenRolesRecursive()
	 */
	public function getChildrenRolesRecursive() : array
	{
		/** @var array<string, AccessRole> $roles */
		$roles = [];
		
		$stack = $this->_record->accessRoleChildren;
		
		while(!empty($stack))
		{
			/** @var AccessRole $current */
			$current = \array_shift($stack);
			if(isset($roles[$current->access_role_id]))
			{
				continue;
			}
			
			$roles[$current->access_role_id] = $current;
			$stack = \array_merge($stack, $current->accessRoleChildren);
		}
		
		$newRoles = [];
		
		foreach($roles as $role)
		{
			$newRoles[] = new RbacRole($role);
		}
		
		return $newRoles;
	}
	
}
