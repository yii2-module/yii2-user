<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use DateInterval;
use DateTimeImmutable;
use InvalidArgumentException;
use PhpExtended\Uuid\UuidV6Factory;
use Psr\Log\LoggerInterface;
use Throwable;
use yii\BaseYii;
use Yii2Module\Yii2User\Forms\SignupForm;
use Yii2Module\Yii2User\Models\UserAccount;
use Yii2Module\Yii2User\Models\UserUser;

/**
 * UserUserManager class file.
 * 
 * This class represents a manager for User records.
 * 
 * @author Anastaszor
 */
class UserUserManager
{
	
	public const USER_ACCOUNT_CONFIRMATION_ID = 'CONFIRMATION';
	public const USER_ACCOUNT_PROVIDER_ID = 'PASSWORD';
	
	public const USER_STATUS_ACTIVE = 'ACT';
	public const USER_STATUS_BANNED = 'BAN';
	public const USER_STATUS_INACTIVE = 'INA';
	public const USER_STATUS_PENDING = 'PEN';
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new UserUserManager with the given dependancies.
	 * 
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Removes all the unconfirmed users that are here from more than 24h.
	 * 
	 * @return integer
	 */
	public function cleanupUnconfirmedUsers() : int
	{
		$query = UserUser::find()->andWhere(['datetime_confirmed_at' => null]);
		$count = 0;
		
		/** @var UserUser $user */
		foreach($query->each() as $user)
		{
			try
			{
				$count += (int) $user->delete();
			}
			catch(Throwable $exc)
			{
				$message = '';
				$previous = $exc;
				
				do
				{
					$message .= $previous->getMessage()."\n\n".$previous->getTraceAsString()."\n\n\n\n";
					$previous = $previous->getPrevious();
				}
				while(null !== $previous);
				
				$this->_logger->error($message);
			}
		}
		
		return $count;
	}
	
	/**
	 * Registers a new User from the signup form. Returns null if the user
	 * cannot be registered.
	 * 
	 * @param SignupForm $form
	 * @param boolean $useEmails
	 * @return ?UserUser
	 * @throws \Random\RandomException
	 * @throws InvalidArgumentException if the user already exists
	 */
	public function registerUser(SignupForm $form, bool $useEmails) : ?UserUser
	{
		$emailHash = (string) \hash('sha512', (string) $form->email);
		$user = UserUser::findOne(['email_hash' => $emailHash]);
		if(null !== $user)
		{
			// user already existant, should not be reinserted
			throw new InvalidArgumentException((string) $form->email);
		}
		
		$uuidFactory = new UuidV6Factory();
		
		$user = new UserUser();
		$user->user_user_id = $uuidFactory->create()->getCanonicalRepresentation();
		$user->username = (string) $form->email;
		$user->username_hash = (string) \hash('sha512', (string) $user->username);
		$user->email_canonical = (string) \mb_strtolower((string) $form->email);
		$user->email_hash = $emailHash;
		$user->user_status_id = $useEmails ? static::USER_STATUS_PENDING : static::USER_STATUS_ACTIVE;
		if(!$useEmails)
		{
			$user->datetime_confirmed_at = (new DateTimeImmutable())->format('Y-m-d H:i:s');
		}
		/** @var \yii\web\Request $request */
		$request = BaseYii::$app->request;
		$user->registered_ip = $request->getUserIP() ?? '(unknown)';
		
		$userAccount = new UserAccount();
		$userAccount->user_account_id = $uuidFactory->create()->getCanonicalRepresentation();
		$userAccount->user_user_id = $user->user_user_id;
		$userAccount->provider_name = self::USER_ACCOUNT_PROVIDER_ID;
		$userAccount->user_provider_id = __CLASS__;
		$userAccount->access_token = (string) \password_hash((string) $form->password, \PASSWORD_BCRYPT, ['cost' => 16]);
		/** @psalm-suppress PossiblyFalseArgument */
		$userAccount->token_expires_at = (new DateTimeImmutable())->add(DateInterval::createFromDateString('+2 years'))->format('Y-m-d H:i:s');
		
		$userConfirm = new UserAccount();
		$userConfirm->user_account_id = $uuidFactory->create()->getCanonicalRepresentation();
		$userConfirm->user_user_id = $user->user_user_id;
		$userConfirm->provider_name = self::USER_ACCOUNT_CONFIRMATION_ID;
		$userConfirm->user_provider_id = __CLASS__;
		$userConfirm->access_token = (string) \mb_substr((string) \hash('sha512', \random_bytes(20).((string) $userAccount->user_account_id)), 100);
		/** @psalm-suppress PossiblyFalseArgument */
		$userConfirm->token_expires_at = (new DateTimeImmutable())->add(DateInterval::createFromDateString('+1 hour'))->format('Y-m-d H:i:s');
		
		try
		{
			$transaction = $user->getDb()->beginTransaction();
			
			foreach([$user, $userAccount, $userConfirm] as $record)
			{
				$record->save();
			}
			
			if($transaction->commit())
			{
				return $user;
			}
		}
		catch(Throwable $exc)
		{
			$this->_logger->error('Failed to register user : {exc}', [
				'exc' => $exc->getMessage(),
			]);
		}
		
		$errors = [];
		
		foreach($user->getErrorSummary(true) as $error)
		{
			$errors[] = (string) $error;
		}
		
		$this->_logger->error('Failed to register user : {errors}', [
			'errors' => \implode("\n", $errors),
		]);
		
		return null;
	}
	
}
