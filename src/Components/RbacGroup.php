<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use PhpExtended\Rbac\GroupInterface;
use PhpExtended\Rbac\GroupStatusInterface;
use Yii2Module\Yii2User\Models\GroupGroup;

/**
 * RbacGroup class file.
 * 
 * This class is a simple implementation based on the records of this module.
 * 
 * @author Anastaszor
 */
class RbacGroup implements GroupInterface
{
	
	public const VISIBILITY_PUBLIC = 'PUB';
	public const VISIBILITY_PRIVATE = 'PRV';
	public const VISIBILITY_SECRET = 'SEC';
	
	/**
	 * The underlying record.
	 * 
	 * @var GroupGroup
	 */
	protected GroupGroup $_record;
	
	/**
	 * Builds a new RbacGroup with the underlying record.
	 * 
	 * @param GroupGroup $record
	 */
	public function __construct(GroupGroup $record)
	{
		$this->_record = $record;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->getIdentifier();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return (string) $this->_record->group_group_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getGroupname()
	 */
	public function getGroupname() : string
	{
		return (string) $this->_record->libelle_en;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getGroupStatus()
	 */
	public function getGroupStatus() : GroupStatusInterface
	{
		return new RbacGroupStatus($this->_record->groupStatus);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getAssignedUsers()
	 */
	public function getAssignedUsers() : array
	{
		$users = [];
		
		foreach($this->_record->userUsers as $user)
		{
			$users[] = new RbacUser($user);
		}
		
		return $users;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getAssignedRoles()
	 */
	public function getAssignedRoles() : array
	{
		$roles = [];
		
		foreach($this->_record->accessRoles as $role)
		{
			$roles[] = new RbacRole($role);
		}
		
		return $roles;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getParentGroups()
	 */
	public function getParentGroups() : array
	{
		$groups = [];
		
		foreach($this->_record->groupParents as $group)
		{
			$groups[] = new RbacGroup($group);
		}
		
		return $groups;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getParentGroupsRecursive()
	 */
	public function getParentGroupsRecursive() : array
	{
		/** @var array<string, GroupGroup> $groups */
		$groups = [];
		
		$stack = $this->_record->groupParents;
		
		while(!empty($stack))
		{
			/** @var GroupGroup $current */
			$current = \array_shift($stack);
			if(isset($groups[$current->group_group_id]))
			{
				continue;
			}
			
			$groups[$current->group_group_id] = $current;
			$stack = \array_merge($stack, $current->groupParents);
		}
		
		$newGroups = [];
		
		foreach($groups as $group)
		{
			$newGroups[] = new RbacGroup($group);
		}
		
		return $newGroups;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getChildrenGroups()
	 */
	public function getChildrenGroups() : array
	{
		$groups = [];
		
		foreach($this->_record->groupChildren as $group)
		{
			$groups[] = new RbacGroup($group);
		}
		
		return $groups;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getChildrenGroupsRecursive()
	 */
	public function getChildrenGroupsRecursive() : array
	{
		/** @var array<string, GroupGroup> $groups */
		$groups = [];
		
		$stack = $this->_record->groupChildren;
		
		while(!empty($stack))
		{
			/** @var GroupGroup $current */
			$current = \array_shift($stack);
			if(isset($groups[$current->group_group_id]))
			{
				continue;
			}
			
			$groups[$current->group_group_id] = $current;
			$stack = \array_merge($stack, $current->groupChildren);
		}
		
		$newGroups = [];
		
		foreach($groups as $group)
		{
			$newGroups[] = new RbacGroup($group);
		}
		
		return $newGroups;
	}
	
}
