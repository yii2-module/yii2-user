<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use PhpExtended\Rbac\GroupStatusInterface;
use Yii2Module\Yii2User\Models\GroupStatus;

/**
 * RbacGroupStatus class file.
 * 
 * This class is a simple implementation based on the records of this module.
 * 
 * @author Anastaszor
 */
class RbacGroupStatus implements GroupStatusInterface
{
	
	public const STATUS_ACTIVE = 'ACT';
	public const STATUS_BANNED = 'BAN';
	public const STATUS_INACTIVE = 'INA';
	public const STATUS_PENDING = 'PEN';
	
	/**
	 * The underlying record.
	 * 
	 * @var GroupStatus
	 */
	protected GroupStatus $_record;
	
	/**
	 * Builds a new RbacGroupStatus with the underlying record.
	 * 
	 * @param GroupStatus $record
	 */
	public function __construct(GroupStatus $record)
	{
		$this->_record = $record;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->getIdentifier();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupStatusInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return (string) $this->_record->group_status_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupStatusInterface::isActive()
	 */
	public function isActive() : bool
	{
		return 'ACT' === $this->_record->group_status_id;
	}
	
}
