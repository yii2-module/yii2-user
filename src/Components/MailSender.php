<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use DateInterval;
use DateMalformedIntervalStringException;
use DateTimeImmutable;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Throwable;
use yii\BaseYii;
use Yii2Module\Helper\Components\ObjectUpdater;
use Yii2Module\Yii2User\Models\MailQueue;

/**
 * MailSender class file.
 * 
 * This class processes registered emails and tries to send them.
 * 
 * @author Anastaszor
 */
class MailSender extends ObjectUpdater
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new MailSender with the given dependancies.
	 * 
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->_logger = $logger;
	}
	
	/**
	 * Processed the current pending emails by locking them so multiple sends
	 * cannot overlap.
	 * 
	 * @param integer $pid
	 * @return integer the number of records executed
	 * @throws \yii\base\NotSupportedException
	 * @throws DateMalformedIntervalStringException
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function processPendingEmails(int $pid) : int
	{
		// first : locks the current records that are eligible
		// are eligible the records that :
		// - have no current pid (= are not currently processed)
		// - datetime_send is not null (= were not successfully sent)
		// - datetime_next_attempt is past
		// - cur_attemps < max_attempts
		MailQueue::updateAll(['cur_thread_pid' => $pid], [
			'cur_thread_pid' => null,
			'datetime_sent' => null,
			'datetime_next_attempt' < \date('Y-m-d H:i:s'),
			'cur_attempts < max_attempts',
		]);
		
		$mailer = BaseYii::$app->mailer;
		$count = 0;
		// second, process each item that is reserved this way
		$queue = MailQueue::find()->andWhere(['cur_thread_pid' => $pid])->each();

		/** @var MailQueue $mail */
		foreach($queue as $mail)
		{
			$mail->cur_thread_pid = null;
			$mail->cur_attempts++;
			$mail->datetime_next_attempt = (new DateTimeImmutable())->add(new DateInterval('P0DT'.((string) $mail->attempt_interval).'S'))->format('Y-m-d H:i:s');
			
			try
			{
				$message = $mailer->compose()
					->setFrom($mail->email_from)
					->setReplyTo($mail->email_from)
					->setTo($mail->email_to)
					->setSubject($mail->subject)
					->setHtmlBody($mail->message)
				;
				
				if($mailer->send($message))
				{
					$mail->datetime_sent = \date('Y-m-d H:i:s');
					$count++;
				}
			}
			/** @phpstan-ignore-next-line */
			catch(Throwable $exc)
			{
				$message = 'Failed to send email {id} to {to}';
				$context = [
					'id' => $mail->mail_queue_id,
					'to' => $mail->email_to,
				];
				$this->_logger->error($message, $context);
			}
			
			$this->saveObject($mail, [], [
				'datetime_last_attempt' => \date('Y-m-d H:i:s'),
			]);
		}
		
		return $count;
	}
	
}
