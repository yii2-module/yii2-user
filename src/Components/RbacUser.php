<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use PhpExtended\Rbac\UserInterface;
use PhpExtended\Rbac\UserStatusInterface;
use Yii2Module\Yii2User\Models\UserUser;

/**
 * RbacUser class file.
 * 
 * This class is a simple implementation based on the records of this module.
 * 
 * @author Anastaszor
 */
class RbacUser implements UserInterface
{
	
	/**
	 * The underlying record.
	 * 
	 * @var UserUser
	 */
	protected UserUser $_record;
	
	/**
	 * Builds a new RbacUser with the underlying record.
	 * 
	 * @param UserUser $record
	 */
	public function __construct(UserUser $record)
	{
		$this->_record = $record;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->getIdentifier();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return (string) $this->_record->user_user_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserInterface::getUsername()
	 */
	public function getUsername() : string
	{
		return (string) $this->_record->username;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserInterface::getUserStatus()
	 */
	public function getUserStatus() : UserStatusInterface
	{
		return new RbacUserStatus($this->_record->userStatus);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserInterface::getAssignedGroups()
	 */
	public function getAssignedGroups() : array
	{
		$groups = [];
		
		foreach($this->_record->groupGroups as $group)
		{
			$groups[] = new RbacGroup($group);
		}
		
		return $groups;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserInterface::getAssignedRoles()
	 */
	public function getAssignedRoles() : array
	{
		$roles = [];
		
		foreach($this->_record->accessRoles as $role)
		{
			$roles[] = new RbacRole($role);
		}
		
		return $roles;
	}
	
}
