<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use yii\base\Component;
use yii\web\IdentityInterface;
use Yii2Module\Yii2User\Models\UserUser;

/**
 * UserIdentitity class file.
 * 
 * This class represents a wrapper for the identity class that uses the
 * underlying UserUser class and table.
 * 
 * @author Anastaszor
 * @todo implement get by access token
 */
class UserIdentity extends Component implements IdentityInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \yii\web\IdentityInterface::findIdentity()
	 */
	public static function findIdentity($id)
	{
		$record = UserUser::findOne($id);
		if(null === $record)
		{
			return null;
		}
		
		return new self($record);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\web\IdentityInterface::findIdentityByAccessToken()
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return null;
	}
	
	/**
	 * The user record.
	 * 
	 * @var UserUser
	 */
	protected UserUser $_record;
	
	/**
	 * Builds a new UserIdentity wrapper around the user record.
	 * 
	 * @param UserUser $user
	 */
	public function __construct(UserUser $user)
	{
		$this->_record = $user;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\web\IdentityInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_record->user_user_id;
	}
	
	/**
	 * Gets the username of the user.
	 * 
	 * @return string
	 */
	public function getUsername() : string
	{
		return $this->_record->username;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\web\IdentityInterface::getAuthKey()
	 */
	public function getAuthKey() : ?string
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\web\IdentityInterface::validateAuthKey()
	 */
	public function validateAuthKey($authKey) : ?bool
	{
		return false;
	}
	
}
