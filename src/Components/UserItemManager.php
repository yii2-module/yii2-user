<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use Stringable;
use Yii2Module\Yii2User\Models\AccessStatus;
use Yii2Module\Yii2User\Models\GroupStatus;
use Yii2Module\Yii2User\Models\GroupVisibility;
use Yii2Module\Yii2User\Models\UserStatus;

/**
 * UserItemManager class file.
 * 
 * This manager manages the global hierarchy of items.
 * 
 * @author Anastaszor
 */
class UserItemManager implements Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Updates all the missing items.
	 * 
	 * @return integer the number of items updated
	 * @throws \yii\db\Exception
	 */
	public function updateDefaultItems() : int
	{
		$added = 0;
		$added += (int) $this->addUserStatus(RbacUserStatus::STATUS_ACTIVE, 'Active');
		$added += (int) $this->addUserStatus(RbacUserStatus::STATUS_BANNED, 'Banned');
		$added += (int) $this->addUserStatus(RbacUserStatus::STATUS_INACTIVE, 'Inactive');
		$added += (int) $this->addUserStatus(RbacUserStatus::STATUS_PENDING, 'Pending');
		$added += (int) $this->addGroupStatus(RbacGroupStatus::STATUS_ACTIVE, 'Active');
		$added += (int) $this->addGroupStatus(RbacGroupStatus::STATUS_BANNED, 'Banned');
		$added += (int) $this->addGroupStatus(RbacGroupStatus::STATUS_INACTIVE, 'Inactive');
		$added += (int) $this->addGroupStatus(RbacGroupStatus::STATUS_PENDING, 'Pending');
		$added += (int) $this->addGroupVisibility(RbacGroup::VISIBILITY_PUBLIC, 'Public');
		$added += (int) $this->addGroupVisibility(RbacGroup::VISIBILITY_PRIVATE, 'Private');
		$added += (int) $this->addGroupVisibility(RbacGroup::VISIBILITY_SECRET, 'Secret');
		$added += (int) $this->addRoleStatus(RbacRoleStatus::STATUS_ACTIVE, 'Active');
		$added += (int) $this->addRoleStatus(RbacRoleStatus::STATUS_INACTIVE, 'Inactive');
		
		return $added;
	}
	
	/**
	 * Adds the given user status.
	 * 
	 * @param string $id
	 * @param string $libelleEn
	 * @return boolean true if success
	 * @throws \yii\db\Exception
	 */
	public function addUserStatus(string $id, string $libelleEn) : bool
	{
		$userStatus = UserStatus::findOne($id);
		if(null === $userStatus)
		{
			$userStatus = new UserStatus();
			$userStatus->user_status_id = $id;
		}
		$userStatus->libelle_en = $libelleEn;
		
		return $userStatus->save();
	}
	
	/**
	 * Adds the given group status.
	 * 
	 * @param string $id
	 * @param string $libelleEn
	 * @return boolean true if success
	 * @throws \yii\db\Exception
	 */
	public function addGroupStatus(string $id, string $libelleEn) : bool
	{
		$groupStatus = GroupStatus::findOne($id);
		if(null === $groupStatus)
		{
			$groupStatus = new GroupStatus();
			$groupStatus->group_status_id = $id;
		}
		$groupStatus->libelle_en = $libelleEn;
		
		return $groupStatus->save();
	}
	
	/**
	 * Adds the given group visibility.
	 * 
	 * @param string $id
	 * @param string $libelleEn
	 * @return boolean true if success
	 * @throws \yii\db\Exception
	 */
	public function addGroupVisibility(string $id, string $libelleEn) : bool
	{
		$groupVisibility = GroupVisibility::findOne($id);
		if(null === $groupVisibility)
		{
			$groupVisibility = new GroupVisibility();
			$groupVisibility->group_visibility_id = $id;
		}
		$groupVisibility->libelle_en = $libelleEn;
		
		return $groupVisibility->save();
	}
	
	/**
	 * Adds the given role status.
	 * 
	 * @param string $id
	 * @param string $libelleEn
	 * @return boolean true if success
	 * @throws \yii\db\Exception
	 */
	public function addRoleStatus(string $id, string $libelleEn) : bool
	{
		$roleStatus = AccessStatus::findOne($id);
		if(null === $roleStatus)
		{
			$roleStatus = new AccessStatus();
			$roleStatus->access_status_id = $id;
		}
		$roleStatus->libelle_en = $libelleEn;
		
		return $roleStatus->save();
	}
	
}
