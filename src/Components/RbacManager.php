<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use DateTimeImmutable;
use PhpExtended\Rbac\SimpleAccessor;
use RuntimeException;
use yii\base\InvalidConfigException;
use yii\BaseYii;
use yii\rbac\Assignment;
use yii\rbac\BaseManager;
use yii\rbac\Item;
use yii\rbac\ManagerInterface;
use yii\rbac\Permission;
use yii\rbac\Role;
use Yii2Module\Yii2User\Models\AccessGroupAssignment;
use Yii2Module\Yii2User\Models\AccessHierarchy;
use Yii2Module\Yii2User\Models\AccessRole;
use Yii2Module\Yii2User\Models\AccessRule;
use Yii2Module\Yii2User\Models\AccessRuleAssignment;
use Yii2Module\Yii2User\Models\AccessUserAssignment;
use Yii2Module\Yii2User\Models\UserUser;

/**
 * RbacManager class file.
 * 
 * This is a simple implementation of the ManagerInterface based on the records
 * of this module.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class RbacManager extends BaseManager implements ManagerInterface
{
	
	/**
	 * The data manager to provide access data.
	 * 
	 * @var RbacProvider
	 */
	protected RbacProvider $_provider;
	
	/**
	 * The accessor to check for access grants.
	 * 
	 * @var SimpleAccessor
	 */
	protected SimpleAccessor $_accessor;
	
	/**
	 * Builds a new RbacManager with the given rbac provider.
	 */
	public function __construct()
	{
		$this->_provider = new RbacProvider();
		$this->_accessor = new SimpleAccessor($this->_provider);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\CheckAccessInterface::checkAccess()
	 * @param string|integer $userId
	 * @param string $permissionName
	 * @param array<integer|string, null|boolean|integer|float|string> $params
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function checkAccess($userId, $permissionName, $params = [])
	{
		$nparams = [];
		
		foreach($params as $key => $value)
		{
			$nparams[(string) $key] = (string) $value;
		}
		
		try
		{
			return $this->_accessor->checkAccess((string) $userId, $permissionName, $nparams);
		}
		catch(\PhpExtended\Rbac\UnprovidableThrowable $exc)
		{
			BaseYii::error($exc->getMessage()."\n\n".$exc->getTraceAsString());
			
			return false;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getRolesByUser()
	 * @throws \yii\base\InvalidParamException
	 * @throws \yii\base\UnknownMethodException
	 * @throws InvalidConfigException
	 */
	public function getRolesByUser($userId)
	{
		$accessRoles = AccessRole::find()->with('accessUserAssignments')->andWhere([
			'user_user_id' => $userId,
		])->all();
		
		$roles = [];
		
		/** @var AccessRole $accessRole */
		foreach($accessRoles as $accessRole)
		{
			if($accessRole->getAccessRoleChildren()->count() > 0)
			{
				$role = new Role();
				$role->name = $accessRole->access_role_id;
				$role->description = $accessRole->libelle_en;
				$role->ruleName = '';
				$role->data = [];
				$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $accessRole->meta_created_at);
				$role->createdAt = false === $dti ? \time() : $dti->getTimestamp();
				$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $accessRole->meta_updated_at);
				$role->updatedAt = false === $dti ? \time() : $dti->getTimestamp();
				$roles[$accessRole->access_role_id] = $role;
				
				foreach($this->getChildRoles($accessRole->access_role_id) as $toBeMerged)
				{
					$roles[$toBeMerged->name] = $toBeMerged;
				}
			}
		}
		
		return $roles;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getChildRoles()
	 * @throws \yii\base\UnknownMethodException
	 * @throws InvalidConfigException
	 */
	public function getChildRoles($roleName)
	{
		$accessRole = AccessRole::findOne($roleName);
		if(null === $accessRole)
		{
			return [];
		}
		
		$roles = [];
		
		/** @var AccessRole $childRole */
		foreach($accessRole->accessRoleChildren as $childRole)
		{
			if($childRole->getAccessRoleChildren()->count() > 0)
			{
				$role = new Role();
				$role->name = $childRole->access_role_id;
				$role->description = $childRole->libelle_en;
				$role->ruleName = '';
				$role->data = [];
				$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $childRole->meta_created_at);
				$role->createdAt = false === $dti ? \time() : $dti->getTimestamp();
				$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $childRole->meta_updated_at);
				$role->updatedAt = false === $dti ? \time() : $dti->getTimestamp();
				$roles[$childRole->access_role_id] = $role;
				
				foreach($this->getChildRoles($childRole->access_role_id) as $toBeMerged)
				{
					$roles[$toBeMerged->name] = $toBeMerged;
				}
			}
		}
		
		return $roles;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getPermissionsByRole()
	 * @throws \yii\base\UnknownMethodException
	 * @throws InvalidConfigException
	 */
	public function getPermissionsByRole($roleName)
	{
		$accessRole = AccessRole::findOne($roleName);
		if(null === $accessRole)
		{
			return [];
		}
		
		$permissions = [];
		
		/** @var AccessRole $accessChild */
		foreach($accessRole->accessRoleChildren as $accessChild)
		{
			if($accessChild->getAccessRoleChildren()->count() > 0)
			{
				foreach($this->getPermissionsByRole($accessChild->access_role_id) as $toBeMerged)
				{
					$permissions[$toBeMerged->name] = $toBeMerged;
				}
				
				continue;
			}
			
			$permission = new Permission();
			$permission->name = $accessChild->access_role_id;
			$permission->description = $accessChild->libelle_en;
			$permission->ruleName = '';
			$permission->data = [];
			$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $accessChild->meta_created_at);
			$permission->createdAt = false === $dti ? \time() : $dti->getTimestamp();
			$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $accessChild->meta_updated_at);
			$permission->updatedAt = false === $dti ? \time() : $dti->getTimestamp();
			$permissions[] = $permission;
		}
		
		return $permissions;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getPermissionsByUser()
	 * @throws \yii\base\InvalidParamException
	 * @throws \yii\base\UnknownMethodException
	 * @throws InvalidConfigException
	 */
	public function getPermissionsByUser($userId)
	{
		$roles = $this->getRolesByUser($userId);
		
		$permissions = [];
		
		foreach($roles as $role)
		{
			foreach($this->getPermissionsByRole($role->name) as $permission)
			{
				$permissions[$permission->name] = $permission;
			}
		}
		
		return $permissions;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getRule()
	 */
	public function getRule($name)
	{
		$accessRule = AccessRule::findOne($name);
		if(null === $accessRule)
		{
			return null;
		}
		
		return new UserAccessRule($accessRule);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getRules()
	 */
	public function getRules()
	{
		$rules = [];
		
		foreach(AccessRule::findAll([]) as $accessRule)
		{
			$rules[$accessRule->access_rule_id] = new UserAccessRule($accessRule);
		}
		
		return $rules;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::canAddChild()
	 */
	public function canAddChild($parent, $child)
	{
		return AccessHierarchy::find()->andWhere([
			'access_role_parent_id' => $parent->name,
			'access_role_child_id' => $child->name,
		])->count() === 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::addChild()
	 */
	public function addChild($parent, $child) : bool
	{
		$hierarchy = new AccessHierarchy();
		$hierarchy->access_role_parent_id = $parent->name;
		$hierarchy->access_role_child_id = $child->name;
		
		return $hierarchy->save();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeChild()
	 */
	public function removeChild($parent, $child)
	{
		try
		{
			return (bool) AccessHierarchy::deleteAll([
				'access_role_parent_id' => $parent->name,
				'access_role_child_id' => $child->name,
			]);
		}
		catch(\yii\base\NotSupportedException $exc)
		{
			BaseYii::error($exc->getMessage()."\n\n".$exc->getTraceAsString());
			
			return false;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeChildren()
	 */
	public function removeChildren($parent)
	{
		try
		{
			return (bool) AccessHierarchy::deleteAll([
				'access_role_parent_id' => $parent->name,
			]);
		}
		catch(\yii\base\NotSupportedException $exc)
		{
			BaseYii::error($exc->getMessage()."\n\n".$exc->getTraceAsString());
			
			return false;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::hasChild()
	 */
	public function hasChild($parent, $child)
	{
		return null !== AccessHierarchy::findOne([
			'access_role_parent_id' => $parent->name,
			'access_role_child_id' => $child->name,
		]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getChildren()
	 * @throws \yii\base\UnknownMethodException
	 * @throws InvalidConfigException
	 */
	public function getChildren($name)
	{
		$role = AccessRole::findOne($name);
		if(null === $role)
		{
			return [];
		}
		
		$items = [];
		
		foreach($role->accessRoleChildren as $childRole)
		{
			$item = $childRole->getAccessRoleChildren()->count() === 0
				? new Permission()
				: new Role();
			$item->name = $childRole->access_role_id;
			$item->description = $childRole->libelle_en;
			$rulenames = [];
			
			foreach($childRole->accessRuleAssignments as $ruleAssignments)
			{
				$rulenames[] = $ruleAssignments->access_rule_id;
			}
			
			$item->ruleName = \implode('|', $rulenames);
			$item->data = [];
			$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $childRole->meta_created_at);
			$item->createdAt = false === $dti ? \time() : $dti->getTimestamp();
			$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $childRole->meta_updated_at);
			$item->updatedAt = false === $dti ? \time() : $dti->getTimestamp();
			$items[] = $item;
		}
		
		return $items;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::assign()
	 */
	public function assign($role, $userId)
	{
		try
		{
			$assignment = new AccessUserAssignment();
			$assignment->access_role_id = $role->name;
			$assignment->user_user_id = (string) $userId;
			$assignment->save();
			// then reload the object to get dates
			$assignment = AccessUserAssignment::findOne([
				'access_role_id' => $role->name,
				'user_user_id' => $userId,
			]);
			if(null === $assignment)
			{
				$message = 'Failed to retrieve newly created assignment from role {role} to user {user}';
				$context = ['role' => $role->name, 'user' => $userId];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		catch(\yii\db\Exception $exc)
		{
			$message = 'The role {role} is already assigned to user {user}';
			$context = ['role' => $role->name, 'user' => $userId];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$nlassign = new Assignment();
		$nlassign->userId = $assignment->user_user_id;
		$nlassign->roleName = $role->name;
		$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $assignment->meta_created_at);
		$nlassign->createdAt = false === $dti ? \time() : $dti->getTimestamp();
		
		return $nlassign;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::revoke()
	 */
	public function revoke($role, $userId)
	{
		try
		{
			return (bool) AccessUserAssignment::deleteAll([
				'user_user_id' => $userId,
				'access_role_id' => $role->name,
			]);
		}
		catch(\yii\base\NotSupportedException $exc)
		{
			return false;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::revokeAll()
	 */
	public function revokeAll($userId)
	{
		try
		{
			return (bool) AccessUserAssignment::deleteAll([
				'user_user_id' => $userId,
			]);
		}
		catch(\yii\base\NotSupportedException $exc)
		{
			return false;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getAssignment()
	 */
	public function getAssignment($roleName, $userId)
	{
		$record = AccessUserAssignment::findOne([
			'user_user_id' => $userId,
			'access_role_id' => $roleName,
		]);
		if(null === $record)
		{
			return null;
		}
		
		$assignment = new Assignment();
		$assignment->userId = $record->user_user_id;
		$assignment->roleName = $record->access_role_id;
		$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $record->meta_created_at);
		$assignment->createdAt = false === $dti ? \time() : $dti->getTimestamp();
		
		return $assignment;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getAssignments()
	 */
	public function getAssignments($userId)
	{
		$assignments = [];
		
		$records = AccessUserAssignment::findAll([
			'user_user_id' => $userId,
		]);
		
		foreach($records as $record)
		{
			$assignment = new Assignment();
			$assignment->userId = $record->user_user_id;
			$assignment->roleName = $record->access_role_id;
			$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $record->meta_created_at);
			$assignment->createdAt = false === $dti ? \time() : $dti->getTimestamp();
			$assignments[] = $assignment;
		}
		
		return $assignments;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getUserIdsByRole()
	 * @param string $roleName
	 * @return array<integer, string>
	 */
	public function getUserIdsByRole($roleName)
	{
		$userIds = [];
		
		foreach(UserUser::find()->with('accessUserAssignments')->select('user_user_id')->andWhere([
			'access_role_id' => $roleName,
		])->all() as $user)
		{
			/** @var UserUser $user */
			$userIds[] = $user->user_user_id;
		}
		
		return $userIds;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeAll()
	 */
	public function removeAll() : void
	{
		$this->removeAllAssignments();
		$this->removeAllRules();
		$this->removeAllRoles();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeAllPermissions()
	 */
	public function removeAllPermissions() : void
	{
		$this->removeAllRoles();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeAllRoles()
	 */
	public function removeAllRoles() : void
	{
		try
		{
			AccessGroupAssignment::deleteAll();
			AccessHierarchy::deleteAll();
			AccessRole::deleteAll();
		}
		catch(\yii\base\NotSupportedException $exc)
		{
			BaseYii::error($exc->getMessage()."\n\n".$exc->getTraceAsString());
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeAllRules()
	 */
	public function removeAllRules() : void
	{
		try
		{
			AccessRule::deleteAll();
		}
		catch(\yii\base\NotSupportedException $exc)
		{
			BaseYii::error($exc->getMessage()."\n\n".$exc->getTraceAsString());
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeAllAssignments()
	 */
	public function removeAllAssignments() : void
	{
		try
		{
			AccessUserAssignment::deleteAll();
		}
		catch(\yii\base\NotSupportedException $exc)
		{
			BaseYii::error($exc->getMessage()."\n\n".$exc->getTraceAsString());
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\BaseManager::getItem()
	 * @throws \yii\base\UnknownMethodException
	 */
	protected function getItem($name)
	{
		$accessRole = AccessRole::findOne($name);
		if(null === $accessRole)
		{
			return null;
		}
		
		try
		{
			$item = $accessRole->getAccessRoleChildren()->count() === 0
				? new Permission()
				: new Role();
		}
		catch(InvalidConfigException $exc)
		{
			// should not happen
			BaseYii::error($exc->getMessage()."\n\n".$exc->getTraceAsString());
			
			return null;
		}
		
		$item->name = $accessRole->access_role_id;
		$item->description = $accessRole->libelle_en;
		$item->ruleName = '';
		$item->data = [];
		$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $accessRole->meta_created_at);
		$item->createdAt = false === $dti ? \time() : $dti->getTimestamp();
		$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $accessRole->meta_updated_at);
		$item->updatedAt = false === $dti ? \time() : $dti->getTimestamp();
		
		return $item;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\BaseManager::getItems()
	 * @throws \yii\base\UnknownMethodException
	 * @throws InvalidConfigException
	 */
	protected function getItems($type)
	{
		$permissions = [];
		
		foreach(AccessRole::findAll([]) as $accessRole)
		{
			$gotten = $accessRole->getAccessRoleChildren()->count() === 0
				? Item::TYPE_PERMISSION
				: Item::TYPE_ROLE;
			
			if($type === $gotten)
			{
				$item = Item::TYPE_ROLE === $gotten ? new Role() : new Permission();
				$item->name = $accessRole->access_role_id;
				$item->description = $accessRole->libelle_en;
				$item->ruleName = '';
				$item->data = [];
				$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $accessRole->meta_created_at);
				$item->createdAt = false === $dti ? \time() : $dti->getTimestamp();
				$dti = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $accessRole->meta_updated_at);
				$item->updatedAt = false === $dti ? \time() : $dti->getTimestamp();
				$permissions[] = $item;
			}
		}
		
		return $permissions;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\BaseManager::addItem()
	 */
	protected function addItem($item) : bool
	{
		return $this->updateItem($item->name, $item);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\BaseManager::addRule()
	 */
	protected function addRule($rule) : bool
	{
		return $this->updateRule($rule->name, $rule);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\BaseManager::removeItem()
	 */
	protected function removeItem($item) : bool
	{
		try
		{
			return AccessRole::deleteAll($item->name) > 0;
		}
		catch(\yii\base\NotSupportedException $exc)
		{
			return false;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\BaseManager::removeRule()
	 */
	protected function removeRule($rule) : bool
	{
		try
		{
			return (bool) AccessRule::deleteAll($rule->name);
		}
		catch(\yii\base\NotSupportedException $exc)
		{
			return false;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\BaseManager::updateItem()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	protected function updateItem($name, $item)
	{
		$role = AccessRole::findOne($name);
		if(null === $role)
		{
			$role = new AccessRole();
		}
		$role->access_role_id = $item->name;
		$role->access_status_id = RbacRoleStatus::STATUS_ACTIVE;
		$role->module_role_id = null === BaseYii::$app->module ? 'app' : BaseYii::$app->module->id;
		$role->libelle_en = $item->description;
		$role->min_ring_level = 1;
		
		try
		{
			$transaction = $role->getDb()->beginTransaction();
			if(!$role->save())
			{
				$transaction->rollBack();
				$errors = [];
				
				foreach((array) $role->getErrorSummary(true) as $error)
				{
					$errors[] = (string) $error;
				}
				
				$message = 'Failed to save access role for item {name} : {errors}';
				$context = ['name' => $item->name, 'errors' => \implode(', ', $errors)];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			if(!empty($item->ruleName))
			{
				foreach(\explode('|', $item->ruleName) as $ruleName)
				{
					$assignment = AccessRuleAssignment::findOne([
						'access_role_id' => $role->access_role_id,
						'access_rule_id' => $ruleName,
					]);
					if(null === $assignment)
					{
						$assignment = new AccessRuleAssignment();
						$assignment->access_role_id = $role->access_role_id;
						$assignment->access_rule_id = $ruleName;
					}
					if(!$assignment->save())
					{
						$transaction->rollBack();
						$errors = [];
						
						foreach((array) $assignment->getErrorSummary(true) as $error)
						{
							$errors[] = (string) $error;
						}
						
						$message = 'Failed to save access role assignment for item {name} with rule {rule} : {errors}';
						$context = ['name' => $item->name, 'rule' => $ruleName, 'errors' => \implode(', ', $errors)];
						
						throw new RuntimeException(\strtr($message, $context));
					}
				}
			}
			
			$transaction->commit();
			
			return true;
		}
		catch(\yii\db\Exception $exc)
		{
			$message = 'Failed to save access role for item {name} : {exc}';
			$context = ['name' => $item->name, 'exc' => $exc->getMessage()];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\BaseManager::updateRule()
	 * @SuppressWarnings("PHPMD.ErrorControlOperator")
	 */
	protected function updateRule($name, $rule) : bool
	{
		$arule = AccessRule::findOne($name);
		if(null === $arule)
		{
			$arule = new AccessRule();
		}
		$arule->access_rule_id = $rule->name;
		$arule->module_rule_id = null === BaseYii::$app->module ? 'app' : BaseYii::$app->module->id;
		
		$serialized = @\serialize($rule);
		if(empty($serialized))
		{
			$message = 'Failed to serialize object of class {class}';
			$context = ['class' => \get_class($rule)];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$encoded = \base64_encode($serialized);
		if(empty($encoded))
		{
			$message = 'Failed to base64 encode object of class {class}';
			$context = ['class' => \get_class($rule)];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$jsonData = (string) \json_encode(['rule' => $encoded]);
		if('' === $jsonData)
		{
			$message = 'Failed to json encode object of class {class}';
			$context = ['class' => \get_class($rule)];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		$arule->json_data = $jsonData;
		
		try
		{
			if(!$arule->save())
			{
				$errors = [];
				
				foreach((array) $arule->getErrorSummary(true) as $error)
				{
					$errors[] = (string) $error;
				}
				
				$message = 'Failed to save rule {name} for module {module} : {errors}';
				$context = ['name' => $arule->access_rule_id, 'module' => $arule->module_rule_id, 'errors' => \implode(', ', $errors)];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			return true;
		}
		catch(\yii\db\Exception $exc)
		{
			$message = 'Failed to save rule {name} for module {module} : {exc}';
			$context = ['name' => $arule->access_rule_id, 'module' => $arule->module_rule_id, 'exc' => $exc->getMessage()];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
	}
	
}
