<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use PhpExtended\Rbac\UserStatusInterface;
use Yii2Module\Yii2User\Models\UserStatus;

/**
 * RbacUserStatus class file.
 * 
 * This class is a simple implementation based on the records of this module.
 * 
 * @author Anastaszor
 */
class RbacUserStatus implements UserStatusInterface
{
	
	public const STATUS_ACTIVE = 'ACT';
	public const STATUS_BANNED = 'BAN';
	public const STATUS_INACTIVE = 'INA';
	public const STATUS_PENDING = 'PEN';
	
	/**
	 * The underlying record.
	 * 
	 * @var UserStatus
	 */
	protected UserStatus $_record;
	
	/**
	 * Builds a new RbacUserStatus with the underlying record.
	 * 
	 * @param UserStatus $record
	 */
	public function __construct(UserStatus $record)
	{
		$this->_record = $record;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->getIdentifier();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserStatusInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return (string) $this->_record->user_status_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserStatusInterface::isActive()
	 */
	public function isActive() : bool
	{
		return 'ACT' === $this->_record->user_status_id;
	}
	
}
