<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use Psr\Log\LoggerInterface;
use yii\rbac\ManagerInterface;

/**
 * RbacLoggerManager class file.
 * 
 * This class is a manager that logs every requests that goes through it.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class RbacLoggerManager implements ManagerInterface
{
	
	/**
	 * The logger for this manager.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The inner manager that does the job.
	 * 
	 * @var ManagerInterface
	 */
	protected ManagerInterface $_manager;
	
	/**
	 * Builds a new RbacLoggerManager with the given manager and logger.
	 * 
	 * @param LoggerInterface $logger
	 * @param ManagerInterface $manager
	 */
	public function __construct(LoggerInterface $logger, ManagerInterface $manager)
	{
		$this->_logger = $logger;
		$this->_manager = $manager;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\CheckAccessInterface::checkAccess()
	 * @param string|integer $userId
	 * @param string $permissionName
	 * @param array<integer|string, null|boolean|integer|float|string> $params
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function checkAccess($userId, $permissionName, $params = [])
	{
		$this->_logger->info('Calling '.__METHOD__.' with user id "'.((string) $userId).'", permission name "'.$permissionName.'" and params {params}', ['params' => $params]);
		
		return $this->checkAccess($userId, $permissionName, $params);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::createRole()
	 */
	public function createRole($name)
	{
		$this->_logger->info('Calling '.__METHOD__.' with name "'.$name.'"');
		
		return $this->_manager->createRole($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::createPermission()
	 */
	public function createPermission($name)
	{
		$this->_logger->info('Calling '.__METHOD__.' with name "'.$name.'"');
		
		return $this->_manager->createPermission($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::add()
	 */
	public function add($object)
	{
		$this->_logger->info('Calling '.__METHOD__.' with id "'.$object->name.'"');
		
		return $this->_manager->add($object);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::remove()
	 */
	public function remove($object)
	{
		$this->_logger->info('Calling '.__METHOD__.' with id "'.$object->name.'"');
		
		return $this->_manager->remove($object);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::update()
	 */
	public function update($name, $object)
	{
		$this->_logger->info('Calling '.__METHOD__.' with id "'.$name.'" and object "'.$object->name.'"');
		
		return $this->_manager->update($name, $object);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getRole()
	 */
	public function getRole($name)
	{
		$this->_logger->info('Calling '.__METHOD__.' with name "'.$name.'"');
		
		return $this->_manager->getRole($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getRoles()
	 */
	public function getRoles()
	{
		$this->_logger->info('Calling '.__METHOD__);
		
		return $this->_manager->getRoles();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getRolesByUser()
	 */
	public function getRolesByUser($userId)
	{
		$this->_logger->info('Calling '.__METHOD__.' with user id "'.((string) $userId).'"');
		
		return $this->_manager->getRolesByUser($userId);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getChildRoles()
	 */
	public function getChildRoles($roleName)
	{
		$this->_logger->info('Calling '.__METHOD__.' with rolen id "'.$roleName.'"');
		
		return $this->_manager->getChildRoles($roleName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getPermission()
	 */
	public function getPermission($name)
	{
		$this->_logger->info('Calling '.__METHOD__.' with name "'.$name.'"');
		
		return $this->_manager->getPermission($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getPermissions()
	 */
	public function getPermissions()
	{
		$this->_logger->info('Calling '.__METHOD__);
		
		return $this->getPermissions();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getPermissionsByRole()
	 */
	public function getPermissionsByRole($roleName)
	{
		$this->_logger->info('Calling '.__METHOD__.' with role id "'.$roleName.'"');
		
		return $this->_manager->getPermissionsByRole($roleName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getPermissionsByUser()
	 */
	public function getPermissionsByUser($userId)
	{
		$this->_logger->info('Calling '.__METHOD__.' with user id "'.((string) $userId).'"');
		
		return $this->_manager->getPermissionsByUser($userId);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getRule()
	 */
	public function getRule($name)
	{
		$this->_logger->info('Calling '.__METHOD__.' with name "'.$name.'"');
		
		return $this->_manager->getRule($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getRules()
	 */
	public function getRules()
	{
		$this->_logger->info('Calling '.__METHOD__);
		
		return $this->_manager->getRules();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::canAddChild()
	 */
	public function canAddChild($parent, $child)
	{
		$this->_logger->info('Calling '.__METHOD__.' with parent id "'.$parent->name.'" and child id "'.$child->name.'"');
		
		return $this->_manager->canAddChild($parent, $child);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::addChild()
	 */
	public function addChild($parent, $child)
	{
		$this->_logger->info('Calling '.__METHOD__.' with parent id "'.$parent->name.'" and child id "'.$child->name.'"');
		
		return $this->_manager->addChild($parent, $child);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeChild()
	 */
	public function removeChild($parent, $child)
	{
		$this->_logger->info('Calling '.__METHOD__.' with parent id "'.$parent->name.'" and child id "'.$child->name.'"');
		
		return $this->_manager->removeChild($parent, $child);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeChildren()
	 */
	public function removeChildren($parent)
	{
		$this->_logger->info('Calling '.__METHOD__.' with parent id "'.$parent->name.'"');
		
		return $this->_manager->removeChildren($parent);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::hasChild()
	 */
	public function hasChild($parent, $child)
	{
		$this->_logger->info('Calling '.__METHOD__.' with parent id "'.$parent->name.'" and child id "'.$child->name.'"');
		
		return $this->_manager->hasChild($parent, $child);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getChildren()
	 */
	public function getChildren($name)
	{
		$this->_logger->info('Calling '.__METHOD__.' with name "'.$name.'"');
		
		return $this->_manager->getChildren($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::assign()
	 */
	public function assign($role, $userId)
	{
		$this->_logger->info('Calling '.__METHOD__.' with role id "'.$role->name.'" and user id "'.((string) $userId).'"');
		
		return $this->_manager->assign($role, $userId);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::revoke()
	 */
	public function revoke($role, $userId)
	{
		$this->_logger->info('Calling '.__METHOD__.' with role id "'.$role->name.'" and user id "'.((string) $userId).'"');
		
		return $this->_manager->revoke($role, $userId);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::revokeAll()
	 */
	public function revokeAll($userId)
	{
		$this->_logger->info('Calling '.__METHOD__.' with user id "'.((string) $userId).'"');
		
		return $this->_manager->revokeAll($userId);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getAssignment()
	 */
	public function getAssignment($roleName, $userId)
	{
		$this->_logger->info('Calling '.__METHOD__.' with role name "'.$roleName.'" and user id "'.((string) $userId).'"');
		
		return $this->_manager->getAssignment($roleName, $userId);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getAssignments()
	 */
	public function getAssignments($userId)
	{
		$this->_logger->info('Calling '.__METHOD__.' with user id "'.((string) $userId).'"');
		
		return $this->_manager->getAssignments($userId);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::getUserIdsByRole()
	 * @param string $roleName
	 * @return array<integer, string>
	 */
	public function getUserIdsByRole($roleName)
	{
		$this->_logger->info('Calling '.__METHOD__.' with role name "'.$roleName.'"');
		
		$userIds = [];
		
		foreach($this->_manager->getUserIdsByRole($roleName) as $userId)
		{
			$userIds[] = (string) $userId;
		}
		
		return $userIds;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeAll()
	 */
	public function removeAll() : void
	{
		$this->_logger->info('Calling '.__METHOD__);
		$this->_manager->removeAll();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeAllPermissions()
	 */
	public function removeAllPermissions() : void
	{
		$this->_logger->info('Calling '.__METHOD__);
		$this->_manager->removeAllPermissions();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeAllRoles()
	 */
	public function removeAllRoles() : void
	{
		$this->_logger->info('Calling '.__METHOD__);
		$this->_manager->removeAllRoles();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeAllRules()
	 */
	public function removeAllRules() : void
	{
		$this->_logger->info('Calling '.__METHOD__);
		$this->_manager->removeAllRules();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\ManagerInterface::removeAllAssignments()
	 */
	public function removeAllAssignments() : void
	{
		$this->_logger->info('Calling '.__METHOD__);
		$this->_manager->removeAllAssignments();
	}
	
}
