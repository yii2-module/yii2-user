<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use PhpExtended\Rbac\Group;
use PhpExtended\Rbac\GroupInterface;
use PhpExtended\Rbac\ProviderInterface;
use PhpExtended\Rbac\RoleInterface;
use PhpExtended\Rbac\Rule;
use PhpExtended\Rbac\RuleInterface;
use PhpExtended\Rbac\UnprovidableException;
use PhpExtended\Rbac\UserInterface;
use Throwable;
use Yii2Module\Yii2User\Models\AccessRole;
use Yii2Module\Yii2User\Models\AccessRule;
use Yii2Module\Yii2User\Models\GroupGroup;
use Yii2Module\Yii2User\Models\UserUser;

/**
 * RbacProvider class file.
 * 
 * This class provides all the methods to get records and transform them to
 * suitable rbac classes.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class RbacProvider implements ProviderInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getUser()
	 */
	public function getUser(string $userId) : ?UserInterface
	{
		try
		{
			$user = UserUser::findOne($userId);
			if(null === $user)
			{
				return null;
			}
			
			return new RbacUser($user);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $userId];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getGroupsFromUser()
	 */
	public function getGroupsFromUser(UserInterface $user) : array
	{
		try
		{
			/** @var array<integer, GroupGroup> $records */
			$records = GroupGroup::find()->with('groupUserAssignments')->andWhere([
				'user_user_id' => $user->getIdentifier(),
			])->all();
			
			$users = [];
			
			foreach($records as $record)
			{
				$users[] = new RbacGroup($record);
			}
			
			return $users;
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $user->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRecursiveGroupsFromUser()
	 */
	public function getRecursiveGroupsFromUser(UserInterface $user) : array
	{
		try
		{
			$groups = [];
			
			foreach($this->getGroupsFromUser($user) as $group)
			{
				$groups[$group->getIdentifier()] = $group;
				
				foreach($this->getChildrenGroupsRecursive($group) as $newGroup)
				{
					$groups[$newGroup->getIdentifier()] = $newGroup;
				}
			}
			
			return \array_values($groups);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $user->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRolesFromUser()
	 */
	public function getRolesFromUser(UserInterface $user) : array
	{
		try
		{
			/** @var array<integer, AccessRole> $records */
			$records = AccessRole::find()->with('accessUserAssignments')->andWhere([
				'user_user_id' => $user->getIdentifier(),
			])->all();
			
			$roles = [];
			
			foreach($records as $role)
			{
				$roles[] = new RbacRole($role);
			}
			
			return $roles;
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $user->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRecursiveRolesFromUser()
	 */
	public function getRecursiveRolesFromUser(UserInterface $user) : array
	{
		try
		{
			$roles = [];
			
			foreach($this->getRolesFromUser($user) as $role)
			{
				$roles[$role->getIdentifier()] = $role;
				
				foreach($this->getChildrenRolesRecursive($role) as $child)
				{
					$roles[$child->getIdentifier()] = $child;
				}
			}
			
			return \array_values($roles);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $user->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRolesFromGroupsFromUser()
	 */
	public function getRolesFromGroupsFromUser(UserInterface $user) : array
	{
		try
		{
			$roles = [];
			
			foreach($this->getRecursiveGroupsFromUser($user) as $group)
			{
				foreach($this->getRolesFromGroup($group) as $role)
				{
					$roles[$role->getIdentifier()] = $role;
				}
			}
			
			return \array_values($roles);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $user->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getAllRolesFromUser()
	 */
	public function getAllRolesFromUser(UserInterface $user) : array
	{
		try
		{
			$roles = [];
			
			foreach($this->getRolesFromUser($user) as $role)
			{
				$roles[$role->getIdentifier()] = $role;
			}
			
			foreach($this->getRolesFromGroupsFromUser($user) as $role)
			{
				$roles[$role->getIdentifier()] = $role;
			}
			
			return \array_values($roles);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $user->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	// }}}
	
	// {{{ Group as anchor
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getGroup()
	 */
	public function getGroup(string $groupId) : ?GroupInterface
	{
		try
		{
			$group = GroupGroup::findOne($groupId);
			if(null === $group)
			{
				return null;
			}
			
			return new RbacGroup($group);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $groupId];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getParentGroups()
	 */
	public function getParentGroups(GroupInterface $group) : array
	{
		try
		{
			/** @var array<integer, GroupGroup> $records */
			$records = GroupGroup::find()->with('groupParents')->andWhere([
				'group_group_id' => $group->getIdentifier(),
			])->all();
			
			$groups = [];
			
			foreach($records as $record)
			{
				$groups[] = new RbacGroup($record);
			}
			
			return $groups;
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $group->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getParentGroupsRecursive()
	 */
	public function getParentGroupsRecursive(GroupInterface $group) : array
	{
		try
		{
			/** @var array<string, GroupInterface> $groups */
			$groups = [];
			
			$stack = $group->getParentGroups();
			
			while(!empty($stack))
			{
				/** @var GroupInterface $current */
				$current = \array_shift($stack);
				if(isset($groups[$current->getIdentifier()]))
				{
					continue;
				}
				
				$groups[$current->getIdentifier()] = $current;
				$stack = \array_merge($stack, $current->getParentGroups());
			}
			
			return \array_values($groups);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $group->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getChildrenGroups()
	 */
	public function getChildrenGroups(GroupInterface $group) : array
	{
		try
		{
			/** @var array<integer, GroupGroup> $records */
			$records = GroupGroup::find()->with('groupChildren')->andWhere([
				'group_group_id' => $group->getIdentifier(),
			])->all();
			
			$groups = [];
			
			foreach($records as $record)
			{
				$groups[] = new RbacGroup($record);
			}
			
			return $groups;
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $group->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getChildrenGroupsRecursive()
	 */
	public function getChildrenGroupsRecursive(GroupInterface $group) : array
	{
		try
		{
			/** @var array<string, GroupInterface> $groups */
			$groups = [];
			
			$stack = $group->getChildrenGroups();
			
			while(!empty($stack))
			{
				/** @var GroupInterface $current */
				$current = \array_shift($stack);
				if(isset($groups[$current->getIdentifier()]))
				{
					continue;
				}
				
				$groups[$current->getIdentifier()] = $current;
				$stack = \array_merge($stack, $current->getChildrenGroups());
			}
			
			return \array_values($groups);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $group->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getUsersFromGroup()
	 */
	public function getUsersFromGroup(GroupInterface $group) : array
	{
		try
		{
			/** @var array<integer, UserUser> $records */
			$records = UserUser::find()->with('groupUserAssignments')->andWhere([
				'group_group_id' => $group->getIdentifier(),
			])->all();
			
			$users = [];
			
			foreach($records as $user)
			{
				$users[] = new RbacUser($user);
			}
			
			return $users;
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $group->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRolesFromGroup()
	 */
	public function getRolesFromGroup(GroupInterface $group) : array
	{
		try
		{
			/** @var array<integer, AccessRole> $records */
			$records = AccessRole::find()->with('accessGroupAssignments')->andWhere([
				'access_group_id' => $group->getIdentifier(),
			])->all();
			
			$roles = [];
			
			foreach($records as $role)
			{
				$roles[] = new RbacRole($role);
			}
			
			return $roles;
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $group->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	// }}}
	
	// {{{ Role as anchor
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRole()
	 */
	public function getRole(string $roleId) : ?RoleInterface
	{
		try
		{
			$role = AccessRole::findOne($roleId);
			if(null === $role)
			{
				return null;
			}
			
			return new RbacRole($role);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $roleId];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getParentRoles()
	 */
	public function getParentRoles(RoleInterface $role) : array
	{
		try
		{
			/** @var array<integer, AccessRole> $records */
			$records = AccessRole::find()->with('accessParents')->andWhere([
				'access_role_id' => $role->getIdentifier(),
			])->all();
			
			$roles = [];
			
			foreach($records as $record)
			{
				$roles[] = new RbacRole($record);
			}
			
			return $roles;
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $role->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getParentRolesRecursive()
	 */
	public function getParentRolesRecursive(RoleInterface $role) : array
	{
		try
		{
			/** @var array<string, RoleInterface> $roles */
			$roles = [];
			
			$stack = $role->getParentRoles();
			
			while(!empty($stack))
			{
				/** @var RoleInterface $current */
				$current = \array_shift($stack);
				if(isset($roles[$current->getIdentifier()]))
				{
					continue;
				}
				
				$roles[$current->getIdentifier()] = $current;
				$stack = \array_merge($stack, $current->getParentRoles());
			}
			
			return \array_values($roles);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $role->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getChildrenRoles()
	 */
	public function getChildrenRoles(RoleInterface $role) : array
	{
		try
		{
			/** @var array<integer, AccessRole> $records */
			$records = AccessRole::find()->with(['accessChildren'])->andWhere([
				'access_role_id' => $role->getIdentifier(),
			])->all();
			
			$roles = [];
			
			foreach($records as $record)
			{
				$roles[] = new RbacRole($record);
			}
			
			return $roles;
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $role->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getChildrenRolesRecursive()
	 */
	public function getChildrenRolesRecursive(RoleInterface $role) : array
	{
		try
		{
			/** @var array<string, RoleInterface> $roles */
			$roles = [];
			
			$stack = $role->getChildrenRoles();
			
			while(!empty($stack))
			{
				/** @var RoleInterface $current */
				$current = \array_shift($stack);
				if(isset($roles[$current->getIdentifier()]))
				{
					continue;
				}
				
				$roles[$current->getIdentifier()] = $current;
				$stack = \array_merge($stack, $current->getChildrenRoles());
			}
			
			return \array_values($roles);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $role->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getGroupsFromRole()
	 */
	public function getGroupsFromRole(RoleInterface $role) : array
	{
		try
		{
			$records = GroupGroup::find()->with('accessRoles')->andWhere([
				'access_role_id' => $role->getIdentifier(),
			])->all();
			
			$groups = [];
			
			/** @var GroupGroup $group */
			foreach($records as $group)
			{
				$groups[] = new RbacGroup($group);
			}
			
			return $groups;
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $role->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getUsersFromRole()
	 */
	public function getUsersFromRole(RoleInterface $role) : array
	{
		try
		{
			$records = UserUser::find()->with('accessRoles')->andWhere([
				'access_role_id' => $role->getIdentifier(),
			])->all();
			
			$users = [];
			
			/** @var UserUser $user */
			foreach($records as $user)
			{
				$users[] = new RbacUser($user);
			}
			
			return $users;
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $role->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRulesFromRole()
	 */
	public function getRulesFromRole(RoleInterface $role) : array
	{
		try
		{
			$records = AccessRule::findAll(['access_role_id' => $role->getIdentifier()]);
			$rules = [];
			
			/** @var AccessRule $rule */
			foreach($records as $rule)
			{
				$decoded = (array) \json_decode($rule->json_data);
				$data = [];
				
				foreach($decoded as $key => $value)
				{
					$data[(string) $key] = (string) $value;
				}
				$rules[] = new Rule(
					$rule->access_rule_id,
					$rule->module_rule_id.'/'.$rule->access_rule_id,
					$data,
				);
			}
			
			return $rules;
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $role->getIdentifier()];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
	// }}}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRule()
	 */
	public function getRule(string $ruleId) : ?RuleInterface
	{
		try
		{
			$rule = AccessRule::findOne($ruleId);
			if(null === $rule)
			{
				return null;
			}
			
			$decoded = (array) \json_decode($rule->json_data);
			$data = [];
			
			foreach($decoded as $key => $value)
			{
				$data[(string) $key] = (string) $value;
			}
			
			return new Rule(
				$rule->access_rule_id,
				$rule->module_rule_id.'/'.$rule->access_rule_id,
				$data,
			);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to {method} with id {id}';
			$context = ['method' => __METHOD__, 'id' => $ruleId];
			
			throw new UnprovidableException(\strtr($message, $context), -1, $exc);
		}
	}
	
}
