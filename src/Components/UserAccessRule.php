<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use DateTimeImmutable;
use Throwable;
use yii\rbac\Item;
use yii\rbac\Rule;
use Yii2Module\Yii2User\Models\AccessRule;

/**
 * UserAccessRule class file.
 * 
 * This class represents an access rule that filters the users and roles.
 * 
 * @author Anastaszor
 */
class UserAccessRule extends Rule
{
	
	/**
	 * The params for this rule.
	 * 
	 * @var array<integer|string, null|boolean|integer|float|string>
	 */
	protected array $_params = [];
	
	/**
	 * Builds a new UserAccessRule with the given rule record.
	 * 
	 * @param AccessRule $accessRule
	 */
	public function __construct(AccessRule $accessRule)
	{
		$this->name = $accessRule->access_rule_id;
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress DocblockTypeContradiction,PossiblyFalseReference */
		$this->createdAt = null === $accessRule->meta_created_at ? \time() : DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $accessRule->meta_created_at)->getTimestamp();
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress DocblockTypeContradiction,PossiblyFalseReference */
		$this->updatedAt = null === $accessRule->meta_updated_at ? \time() : DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $accessRule->meta_updated_at)->getTimestamp();
		
		/** @psalm-suppress MixedPropertyTypeCoercion */
		$this->_params = (array) \json_decode($accessRule->json_data, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\rbac\Rule::execute()
	 * @param string|integer $user
	 * @param Item $item
	 * @param array<integer|string, null|boolean|integer|float|string> $params
	 * @psalm-suppress MoreSpecificImplementedParamType
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ErrorControlOperator")
	 */
	public function execute($user, $item, $params) : bool
	{
		if(isset($this->_params['user']) && $this->_params['user'] !== $user)
		{
			return false;
		}
		
		if(isset($this->_params['role']) && $this->_params['role'] !== $item->name)
		{
			return false;
		}
		
		foreach($params as $key => $value)
		{
			if(!isset($this->_params[$key]))
			{
				return false;
			}
			
			if($this->_params[$key] !== $value)
			{
				return false;
			}
		}
		
		if(isset($this->_params['rule']) && \is_string($this->_params['rule']))
		{
			$unserializable = (string) \base64_decode($this->_params['rule'], true);
			if(\trim($unserializable) === '')
			{
				return false;
			}
			
			$unserialized = @\unserialize($unserializable);
			if(!$unserialized instanceof Rule)
			{
				return false;
			}
			
			try
			{
				return $unserialized->execute($user, $item, $params);
			}
			/** @phpstan-ignore-next-line */
			catch(Throwable $exc)
			{
				return false;
			}
		}
		
		return true;
	}
	
}
