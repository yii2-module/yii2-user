<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Components;

use PhpExtended\Rbac\RoleStatusInterface;
use Yii2Module\Yii2User\Models\AccessStatus;

/**
 * RbacRoleStatus class file.
 * 
 * This class is a simple implementation based on the record of this module.
 * 
 * @author Anastaszor
 */
class RbacRoleStatus implements RoleStatusInterface
{
	
	public const STATUS_ACTIVE = 'ACT';
	public const STATUS_INACTIVE = 'INA';
	
	/**
	 * The underlying record.
	 * 
	 * @var AccessStatus
	 */
	protected AccessStatus $_record;
	
	/**
	 * Builds a new RbacRoleStatus with the underlying record.
	 * 
	 * @param AccessStatus $record
	 */
	public function __construct(AccessStatus $record)
	{
		$this->_record = $record;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.$this->getIdentifier();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleStatusInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return (string) $this->_record->access_status_id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleStatusInterface::isActive()
	 */
	public function isActive() : bool
	{
		return 'ACT' === $this->_record->access_status_id;
	}
	
}
