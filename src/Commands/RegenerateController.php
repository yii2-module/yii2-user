<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Commands;

use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2User\Components\UserItemManager;

/**
 * RegenerateController class file.
 * 
 * This class is a console command to regenerate the default roles and accesses
 * that are needed for this module.
 * 
 * @author Anastaszor
 */
class RegenerateController extends ExtendedController
{
	
	/**
	 * Updates all the default items, adding those who are missing.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionUpdate() : int
	{
		return $this->runCallable(function() : int
		{
			$manager = new UserItemManager(/* $this->getLogger() */);
			$manager->updateDefaultItems();

			return ExitCode::OK;
		});
	}
	
}
