<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Commands;

use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2User\Components\MailSender;

/**
 * MailController class file.
 * 
 * This class is a console command that is to send mails periodically.
 * 
 * @author Anastaszor
 */
class MailController extends ExtendedController
{
	
	/**
	 * Sends all the welcome / registering emails.
	 * 
	 * @return integer
	 */
	public function actionSendMail() : int
	{
		return $this->runCallable(function() : int
		{
			$mailSender = new MailSender($this->getLogger());
			$mailSender->processPendingEmails((int) \getmypid());

			return ExitCode::OK;
		});
	}
	
}
