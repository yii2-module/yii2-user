<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Commands;

use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2User\Components\UserUserManager;

/**
 * CleanupController class file.
 * 
 * This class is a console command that is to clean up records.
 * 
 * @author Anastaszor
 */
class CleanupController extends ExtendedController
{
	
	/**
	 * Removes all the users that were not confirmed after 24h of inactivity.
	 * 
	 * @return integer the error code, 0 if no error
	 */
	public function actionRemoveUnconfirmedUsers() : int
	{
		return $this->runCallable(function() : int
		{
			$manager = new UserUserManager($this->getLogger());
			$manager->cleanupUnconfirmedUsers();

			return ExitCode::OK;
		});
	}
	
}
