<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Models;

use yii\base\InvalidConfigException;
use yii\base\UnknownMethodException;
use yii\BaseYii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;

/**
 * This is the model class for table "user_user".
 *
 * @property string $user_user_id The unique uuid of the user
 * @property string $meta_created_at Metadata for the creation date of this record
 * @property string $meta_updated_at Metadata for the last updated date of this record
 * @property ?string $meta_deleted_at Metadata for the soft deletion date of this record
 * @property string $user_status_id The status of the user
 * @property string $username_hash The sha512 of the username
 * @property string $username The name that has been choosen by the user
 * @property string $email_hash The sha512 of the user email
 * @property string $email_canonical The canonicalized email of the user
 * @property ?string $datetime_confirmed_at The date when the user was confirmed
 * @property string $registered_ip The ip address of the user
 *
 * @property AccessUserAssignment[] $accessUserAssignments
 * @property AccessRole[] $accessRoles
 * @property GroupUserAssignment[] $groupUserAssignments
 * @property GroupGroup[] $groupGroups
 * @property UserAccount[] $userAccounts
 * @property UserStatus $userStatus
 *
 * /!\ WARNING /!\
 * This class is generated by the gii module, please do not edit manually this
 * file as the changes may be overritten.
 *
 * @author Anastaszor
 */
class UserUser extends ActiveRecord
{

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::tableName()
	 */
	public static function tableName() : string
	{
		return 'user_user';
	}

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::getDb()
	 * @return Connection the database connection used by this AR class
	 * @throws InvalidConfigException
	 * @psalm-suppress MoreSpecificReturnType
	 * @psalm-suppress InvalidNullableReturnType
	 */
	public static function getDb() : Connection
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement, NullableReturnStatement */
		return BaseYii::$app->get('db_user');
	}

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::rules()
	 * @return array<integer, array<integer|string, boolean|integer|float|string|array<integer|string, boolean|integer|float|string>>>
	 */
	public function rules() : array
	{
		return [
			[['user_user_id', 'username_hash', 'username', 'email_hash', 'email_canonical'], 'required'],
			[['meta_created_at', 'meta_updated_at', 'meta_deleted_at', 'datetime_confirmed_at'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
			[['user_user_id'], 'string', 'max' => 36],
			[['user_status_id'], 'string', 'max' => 3],
			[['username_hash', 'email_hash'], 'string', 'max' => 128],
			[['username', 'email_canonical'], 'string', 'max' => 50],
			[['registered_ip'], 'string', 'max' => 45],
		];
	}

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::attributeLabels()
	 * @return array<string, string>
	 */
	public function attributeLabels() : array
	{
		return [
			'user_user_id' => BaseYii::t('UserModule.Models', 'User User ID'),
			'meta_created_at' => BaseYii::t('UserModule.Models', 'Meta Created At'),
			'meta_updated_at' => BaseYii::t('UserModule.Models', 'Meta Updated At'),
			'meta_deleted_at' => BaseYii::t('UserModule.Models', 'Meta Deleted At'),
			'user_status_id' => BaseYii::t('UserModule.Models', 'User Status ID'),
			'username_hash' => BaseYii::t('UserModule.Models', 'Username Hash'),
			'username' => BaseYii::t('UserModule.Models', 'Username'),
			'email_hash' => BaseYii::t('UserModule.Models', 'Email Hash'),
			'email_canonical' => BaseYii::t('UserModule.Models', 'Email Canonical'),
			'datetime_confirmed_at' => BaseYii::t('UserModule.Models', 'Datetime Confirmed At'),
			'registered_ip' => BaseYii::t('UserModule.Models', 'Registered Ip'),
		];
	}

	/**
	 * @return ActiveQuery
	 * @throws UnknownMethodException
	 */
	public function getAccessUserAssignments() : ActiveQuery
	{
		return $this->hasMany(AccessUserAssignment::class, ['user_user_id' => 'user_user_id']);
	}

	/**
	 * @return ActiveQuery
	 * @throws InvalidConfigException
	 * @throws UnknownMethodException
	 */
	public function getAccessRoles() : ActiveQuery
	{
		return $this->hasMany(AccessRole::class, ['access_role_id' => 'access_role_id'])->viaTable('access_user_assignment', ['user_user_id' => 'user_user_id']);
	}

	/**
	 * @return ActiveQuery
	 * @throws UnknownMethodException
	 */
	public function getGroupUserAssignments() : ActiveQuery
	{
		return $this->hasMany(GroupUserAssignment::class, ['user_user_id' => 'user_user_id']);
	}

	/**
	 * @return ActiveQuery
	 * @throws InvalidConfigException
	 * @throws UnknownMethodException
	 */
	public function getGroupGroups() : ActiveQuery
	{
		return $this->hasMany(GroupGroup::class, ['group_group_id' => 'group_group_id'])->viaTable('group_user_assignment', ['user_user_id' => 'user_user_id']);
	}

	/**
	 * @return ActiveQuery
	 * @throws UnknownMethodException
	 */
	public function getUserAccounts() : ActiveQuery
	{
		return $this->hasMany(UserAccount::class, ['user_user_id' => 'user_user_id']);
	}

	/**
	 * @return ActiveQuery
	 * @throws UnknownMethodException
	 */
	public function getUserStatus() : ActiveQuery
	{
		return $this->hasOne(UserStatus::class, ['user_status_id' => 'user_status_id']);
	}

}
