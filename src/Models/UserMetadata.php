<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Models;

use yii\base\InvalidConfigException;
use yii\BaseYii;
use yii\db\ActiveRecord;
use yii\db\Connection;

/**
 * This is the model class for table "user_metadata".
 *
 * @property string $user_metadata_id The id of the metadata
 * @property string $contents The value of the setting
 *
 * /!\ WARNING /!\
 * This class is generated by the gii module, please do not edit manually this
 * file as the changes may be overritten.
 *
 * @author Anastaszor
 */
class UserMetadata extends ActiveRecord
{

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::tableName()
	 */
	public static function tableName() : string
	{
		return 'user_metadata';
	}

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::getDb()
	 * @return Connection the database connection used by this AR class
	 * @throws InvalidConfigException
	 * @psalm-suppress MoreSpecificReturnType
	 * @psalm-suppress InvalidNullableReturnType
	 */
	public static function getDb() : Connection
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement, NullableReturnStatement */
		return BaseYii::$app->get('db_user');
	}

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::rules()
	 * @return array<integer, array<integer|string, boolean|integer|float|string|array<integer|string, boolean|integer|float|string>>>
	 */
	public function rules() : array
	{
		return [
			[['user_metadata_id', 'contents'], 'required'],
			[['user_metadata_id', 'contents'], 'string', 'max' => 250],
		];
	}

	/**
	 * {@inheritDoc}
	 * @see \yii\base\Model::attributeLabels()
	 * @return array<string, string>
	 */
	public function attributeLabels() : array
	{
		return [
			'user_metadata_id' => BaseYii::t('UserModule.Models', 'User Metadata ID'),
			'contents' => BaseYii::t('UserModule.Models', 'Contents'),
		];
	}

}
