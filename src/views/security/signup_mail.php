<?php declare(strict_types=1);
use yii\BaseYii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/** @var View $this */
/** @var string $app */
/** @var string $token */
?>
<h1><?php echo BaseYii::t('UserModule.View', 'Welcome to {app}', ['{app}' => $app]); ?></h1>

<p><?php echo BaseYii::t('UserModule.View', ''); ?></p>
<?php echo Html::a('Confirm your Inscription', Url::toRoute(['/user/security/confirm', 'token' => $token])); ?>
