<?php declare(strict_types=1);

use yii\BaseYii;
use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;
use yii\web\View;
use Yii2Module\Yii2User\Forms\SignupForm;

/** @var View $this */
/** @var SignupForm $model */
/** @var ActiveForm $form */
$this->title = BaseYii::t('UserModule.View', 'Register');
$this->params['breadcrumbs'][] = $this->title;
?>
<br><br>
<div class="row">
	<div class="col-md-4 offset-md-4">
		<div class="card card-default">
			<div class="card-body">
				<?php $form = ActiveForm::begin([
					'id' => 'form-signup',
					'options' => ['class' => 'form-vertical'],
				]); ?>
				<?php echo $form->field($model, 'email')->textInput(['autofocus' => true]); ?>
				<?php echo $form->field($model, 'password')->passwordInput(); ?>
				
				<div class="col-md-4 offset-md-4 form-group">
					<?php echo Html::submitButton(BaseYii::t('UserModule.View', 'Signup'), ['class' => 'btn btn-primary', 'name' => 'signup-button']); ?>
				</div>
				<?php $form->end(); ?>
			</div>
		</div>
	</div>
</div>
<br><br><br><br>
