<?php declare(strict_types=1);

/** @var yii\web\View $this */
/** @var ActiveForm $form */
/** @var Yii2Module\Yii2User\Forms\LoginForm $model */

use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<br><br>
<div class="row">
	<div class="col-md-4 offset-md-4">
		<div class="card card-default">
			<div class="card-body">
				<?php $form = ActiveForm::begin([
					'id' => 'login-form',
					'options' => ['class' => 'form-vertical'],
				]); ?>
				<?php echo $form->field($model, 'email')->textInput(['autofocus' => true]); ?>
				<?php echo $form->field($model, 'password')->passwordInput(); ?>
				<?php echo $form->field($model, 'rememberMe')->checkbox(); ?>
				<div class="col-md-4 offset-md-4 form-group">
					<?php echo Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']); ?>
				</div>
				<?php $form->end(); ?>
			</div>
		</div>
	</div>
</div>
<br><br><br><br>
