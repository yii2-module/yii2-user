<?php declare(strict_types=1);

use yii\bootstrap5\ActiveForm;
use yii\web\View;
use Yii2Module\Yii2User\Models\GroupGroup;

/** @var View $this */
/** @var GroupGroup $model */
/** @var ActiveForm $form */
/** @var array<string, string> $statusItems */
/** @var array<string, string> $visibilityItems */
/** @author Anastaszor */
?>
<div class="record-form">

<?php $form = ActiveForm::begin(); ?>

<?php echo $form->field($model, 'group_status_id')->dropDownList($statusItems); ?>
<?php echo $form->field($model, 'group_visibility_id')->dropDownList($visibilityItems); ?>
<?php echo $form->field($model, 'libelle_en')->textInput(['maxlength' => '64']); ?>

</div>
