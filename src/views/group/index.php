<?php declare(strict_types=1);

use yii\BaseYii;
use yii\bootstrap5\Html;
use yii\bootstrap5\LinkPager;
use yii\data\DataProviderInterface;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;

/** @var yii\web\View $this */
/** @var Yii2Module\Yii2User\Models\GroupGroup $model */
/** @var DataProviderInterface $dataProvider */
$this->title = BaseYii::t('UserModule.View', 'Group Index');
$this->params['breadcrumbs'][] = $this->title;
?>
<br><br>
<p><?php echo Html::a(
	BaseYii::t('UserModule.View', 'Create new Group'),
	['group/create'],
	['class' => 'btn btn-success'],
); ?></p>

<?php echo GridView::widget([
	'dataProvider' => $dataProvider,
	'columns' => [
		[
			'class' => SerialColumn::class,
		],
		'group_group_id',
		'group_status_id',
		'group_visibility_id',
		'libelle_en',
		[
			'class' => ActionColumn::class,
		],
	],
	'pager' => [
		'class' => LinkPager::class,
		'options' => [
			'class' => 'pagination',
		],
		'linkContainerOptions' => [
			'class' => 'page-item',
		],
		'linkOptions' => [
			'class' => 'page-link',
		],
		'prevPageLabel' => '&lsaquo;',
		'nextPageLabel' => '&rsaquo;',
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo;',
		'registerLinkTags' => true,
		'disabledListItemSubTagOptions' => [
			'tag' => 'a',
			'class' => 'page-link',
		],
	],
]);
