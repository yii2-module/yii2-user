<?php declare(strict_types=1);

use yii\BaseYii;
use yii\bootstrap5\Html;
use yii\web\View;
use Yii2Module\Yii2User\Models\GroupGroup;

/** @var View $this */
/** @var GroupGroup $model */
/** @var array<string, string> $statusItems */
/** @var array<string, string> $visibilityItems */
$this->title = BaseYii::t('UserModule.View', 'Create Group');
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?php echo Html::encode($this->title); ?></h1>

<?php echo (string) $this->render('_form', [
	'model' => $model,
	'statusItems' => $statusItems,
	'visibilityItems' => $visibilityItems,
]);
