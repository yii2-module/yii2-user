<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Controllers;

use yii\web\Controller;

class DashboardController extends Controller
{
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Component::behaviors()
	 * @return array<string, array<string, string|array<integer, array<string, boolean|integer|float|string|array<integer, string>>>>>
	 */
	public function behaviors() : array
	{
		// TODO refine with RBAC
		return [
			// 			'access' => [
			// 				'class' => AccessControl::class,
			// 				'rules' => [
			// 					[
			// 						'class' => AccessRule::class,
			// 						'actions' => ['*'],
			// 						'allow' => true,
			// 						'roles' => ['@'],
			// 					],
			// 				],
			// 			],
		];
	}
	
	/**
	 * Lists all the available actions to do within the user module.
	 * 
	 * @return string
	 * @throws \yii\base\InvalidArgumentException
	 */
	public function actionIndex()
	{
		return $this->render('index', [
			
		]);
	}
	
}
