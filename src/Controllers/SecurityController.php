<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Controllers;

use DateMalformedIntervalStringException;
use InvalidArgumentException;
use RuntimeException;
use Throwable;
use yii\BaseYii;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\web\Controller;
use yii\web\Request;
use yii\web\Response;
use Yii2Extended\Yii2Log\Psr3ToYii2Logger;
use Yii2Module\Yii2User\Components\UserUserManager;
use Yii2Module\Yii2User\Forms\LoginForm;
use Yii2Module\Yii2User\Forms\SignupForm;
use Yii2Module\Yii2User\Models\UserAccount;
use Yii2Module\Yii2User\UserModule;

/**
 * SecurityController class file.
 * 
 * This class represents the main gateway to the application.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class SecurityController extends Controller
{
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Component::behaviors()
	 * @return array<string, array<string, string|array<integer, array<string, boolean|integer|float|string|array<integer, string>>>>>
	 */
	public function behaviors() : array
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'class' => AccessRule::class,
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'class' => AccessRule::class,
						'actions' => ['confirm', 'login', 'signup'],
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		];
	}
	
	/**
	 * Registration confirmation action.
	 * 
	 * @param string $token
	 * @return Response|string
	 * @throws \yii\base\InvalidArgumentException
	 * @throws \yii\db\StaleObjectException
	 * @throws Throwable
	 */
	public function actionConfirm(string $token)
	{
		/** @var ?UserAccount $account */
		$account = UserAccount::find()->andWhere([
			'provider_name' => UserUserManager::USER_ACCOUNT_CONFIRMATION_ID,
			'access_token' => ':token',
		], [
			':token' => $token,
		]);
		if(null !== $account)
		{
			$user = $account->userUser;
			$user->datetime_confirmed_at = \date('Y-m-d H:i:s');
			$user->save();
			$account->delete();
		}
		
		return $this->goHome();
	}
	
	/**
	 * Login action.
	 *
	 * @return Response|string
	 * @throws \yii\base\InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function actionLogin()
	{
		/** @var \yii\web\Application $app */
		$app = BaseYii::$app;
		if(!$app->user->isGuest)
		{
			return $this->goHome();
		}
		
		/** @var Request $request */
		$request = $app->request;
		$model = new LoginForm();
		if ($model->load((array) $request->post()) && $model->login())
		{	
			return $this->response->redirect([UserModule::getInstance()->routeRedirectAfterLoginSuccess]);
		}
		
		$model->password = null;
		
		return $this->render('login', [
			'model' => $model,
		]);
	}
	
	/**
	 * Logout action.
	 *
	 * @return Response
	 */
	public function actionLogout()
	{
		/** @var \yii\web\Application $app */
		$app = BaseYii::$app;
		$app->user->logout();
		
		return $this->goHome();
	}
	
	/**
	 * Effectively does the user registration.
	 *
	 * @return string|Response
	 * @throws \Random\RandomException
	 * @throws \yii\base\InvalidArgumentException
	 * @throws \yii\base\UnknownMethodException
	 * @throws DateMalformedIntervalStringException
	 * @throws RuntimeException
	 */
	public function actionSignup()
	{
		$form = new SignupForm();
		
		/** @var Request $request */
		$request = BaseYii::$app->request;
		if($form->load((array) $request->post()) && $form->validate())
		{
			$useEmails = (bool) UserModule::getInstance()->useEmails;
			$userManager = new UserUserManager(new Psr3ToYii2Logger());
			
			try
			{
				$user = $userManager->registerUser($form, $useEmails);
				if(null !== $user && $useEmails)
				{
					$userToken = $user->getUserAccounts()->andWhere([
						'provider_name' => UserUserManager::USER_ACCOUNT_CONFIRMATION_ID,
					])->one();
					if(null !== $userToken)
					{
						/** @psalm-suppress PossiblyUndefinedStringArrayOffset */
						$emailTo = (string) BaseYii::$app->params['user.email.from'];
						$userModule = UserModule::getInstance();
						$emailFrom = (string) $userModule->emailFrom;
						$subject = BaseYii::t('UserModule.Controllers', 'Congratulations {name} for registering on {appname}', [
							'{name}' => $user->username,
							'{appname}' => BaseYii::$app->name,
						]);
						$contents = $this->render('signup_mail', [
							'user' => $user,
						]);
						$userModule->enqueueEmail($emailFrom, $emailTo, $subject, $contents);
					}
				}
			}
			catch(InvalidArgumentException $exc)
			{
				// TODO email to alert user that someone tries to register its account
			}
			
			if(!$useEmails)
			{
				/** @var \yii\web\Application $app */
				$app = BaseYii::$app;
				$app->session->addFlash('success', BaseYii::t('UserModule.Module', 'Your account has been created. Please log in with your credentials.'));
			}
			
			return $this->redirect(['security/login']);
		}
		
		return $this->render('signup', [
			'model' => $form,
		]);
	}
	
}
