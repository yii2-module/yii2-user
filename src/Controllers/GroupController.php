<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-user library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2User\Controllers;

use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\Request;
use yii\web\Response;
use Yii2Module\Yii2User\Models\GroupGroup;
use Yii2Module\Yii2User\Models\GroupStatus;
use Yii2Module\Yii2User\Models\GroupVisibility;

/**
 * GroupController class file.
 * 
 * This class represents all the actions that are possible on groups.
 * Those are :
 * - view list of groups (paginated)
 * - view specific group
 * - view users linked to group (paginated)
 * - view subgroups linked to group (paginated)
 * - view supergroups linked to group (paginated)
 * - view accesses linked to group (paginated)
 * - update group visibility
 * - update group status
 * - add child group
 * - remove child group
 * - add parent group
 * - remove parent group
 * - assign access to group
 * - unassign access from group
 * - assign user to group
 * - unassign user from group
 * - delete group
 * 
 * Advanced :
 * - view subgroup hierarchy
 * - view supergroup hierarchy
 * - view users impacted by (super)group hierarchy
 * - view roles impacted by (sub)group hierarchy
 * 
 * @author Anastaszor
 */
class GroupController extends Controller
{
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Component::behaviors()
	 * @return array<string, array<string, string|array<integer, array<string, boolean|integer|float|string|array<integer, string>>>>>
	 */
	public function behaviors() : array
	{
		// TODO refine with RBAC
		return [
			// 			'access' => [
			// 				'class' => AccessControl::class,
			// 				'rules' => [
			// 					[
			// 						'class' => AccessRule::class,
			// 						'actions' => ['*'],
			// 						'allow' => true,
			// 						'roles' => ['@'],
			// 					],
			// 				],
			// 			],
		];
	}
	
	/**
	 * Lists all the available groups, with pagination.
	 * 
	 * @param Request $request the http request
	 * @return Response|string
	 * @throws \yii\base\InvalidArgumentException
	 * @throws \yii\web\ForbiddenHttpException
	 * @throws \yii\web\NotFoundHttpException
	 */
	public function actionIndex(Request $request)
	{
		$model = new GroupGroup();
		$model->setAttributes($request->queryParams);
		
		$query = GroupGroup::find();
		
		return $this->render('index', [
			'model' => $model,
			'dataProvider' => new ActiveDataProvider([
				'query' => $query,
				'pagination' => [
					'class' => Pagination::class,
					'defaultPageSize' => 50,
				],
			]),
		]);
	}
	
	/**
	 * Creates a new group.
	 * 
	 * @param Request $request
	 * @return Response|string
	 * @throws \yii\base\InvalidArgumentException
	 * @throws \yii\db\Exception
	 * @throws \yii\web\ForbiddenHttpException
	 * @throws \yii\web\NotFoundHttpException
	 */
	public function actionCreate(Request $request)
	{
		$model = new GroupGroup();
		
		if($model->load((array) $request->post()) && $model->save())
		{
			return $this->redirect(['view', 'id' => $model->group_group_id]);
		}
		
		$statusItems = [];
		
		/** @var GroupStatus $groupStatus */
		foreach(GroupStatus::find()->all() as $groupStatus)
		{
			$statusItems[$groupStatus->group_status_id] = $groupStatus->libelle_en;
		}
		
		$visibilityItems = [];
		
		/** @var GroupVisibility $groupVisibility */
		foreach(GroupVisibility::find()->all() as $groupVisibility)
		{
			$visibilityItems[$groupVisibility->group_visibility_id] = $groupVisibility->libelle_en;
		}
		
		return $this->render('create', [
			'model' => $model,
			'statusItems' => $statusItems,
			'visibilityItems' => $visibilityItems,
		]);
	}
	
}
